package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class TrueRemoveCards extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueRemoveCards.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By deleteCross = By.xpath("//img[@src='/1c8b8b83606e1ef2fcd2499e256d09a0.svg']");
	By confirmButton = By.id("confirm_button");

	// Method to remove save cards one by one
	public void verifyRemoveCardsOneByOne() throws Exception {
		try {
			if (checkElementPresence(deleteCross)) {
				waitForElement(deleteCross);
				javaScriptScrollUsingBy(deleteCross);
				int retryCount = 0;
				while (checkElementPresence(deleteCross) == true) {
					waitForElement(deleteCross);
					moveToWebElementAndActionClick(deleteCross);
					Thread.sleep(2000);
					moveToWebElementAndActionClick(confirmButton);
					if (checkElementPresence(loaderSpinner)) {
						while (checkElementPresence(loaderSpinner) == true
								&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
							Thread.sleep(200);
							retryCount++;
						}
					}
					retryCount++;
				}
			}

		} catch (Exception e) {
			logger.info("Save cards are not present");
		}
	}
}
