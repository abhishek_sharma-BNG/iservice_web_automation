package com.automation.iService.utils;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.Key;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class CommonUtils extends TestBase {

	static WebDriver driver;
	static final Logger logger = Logger.getLogger(CommonUtils.class.getName());
	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell Cell;
	private static final String ALGO = "AES";
	private static final byte[] keyValue = new byte[] { 0x01, 0x17, 0x23, 0x36, 0x45, 0x55, 0x67, 0x78, 0x18, 0x39,
			0x4a, 0x5b, 0x7c, 0x4d, 0x6e, 0x1f };

	// Download folder path
	static String userHome = System.getProperty("user.home");
	static String outputFolder = userHome + File.separator + "Downloads" + File.separator;

	// This method is to set the File path and to open the Excel file, Pass Excel
	// Path and Sheetname as Arguments to this method

	public CommonUtils() {
		this.driver = getDriver.getDriver();
		// TODO Auto-generated constructor stub
	}

	public static void setExcelFile(String Path, String SheetName) throws Exception {
		try {
			// Open the Excel file
			FileInputStream ExcelFile = new FileInputStream(Path);
			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
		} catch (Exception e) {
			throw (e);
		}
	}

	public static Object[][] getTableArray(String FilePath, String SheetName, int iTestCaseRow) throws Exception {
		String[][] tabArray = null;
		try {
			FileInputStream ExcelFile = new FileInputStream(FilePath);
			// Access the required test data sheet
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			int startCol = 1;
			int ci = 0, cj = 0;
			int totalRows = 1;
			int totalCols = 2;
			tabArray = new String[totalRows][totalCols];
			for (int j = startCol; j <= totalCols; j++, cj++) {
				tabArray[ci][cj] = getCellData(iTestCaseRow, j);
				logger.info(tabArray[ci][cj]);
			}
		} catch (FileNotFoundException e) {
			logger.info("Could not read the Excel sheet");
			logger.info(e.toString());
		} catch (IOException e) {
			logger.info("Could not read the Excel sheet");
			logger.info(e.toString());
		}
		return (tabArray);
	}

	// This method is to read the test data from the Excel cell, in this we are
	// passing parameters as Row num and Col num

	public static String getCellData(int RowNum, int ColNum) throws Exception {
		try {
			Cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);
			String CellData = Cell.getStringCellValue();
			return CellData;
		} catch (Exception e) {
			logger.info(e.toString());
			return "";
		}
	}

	// Method to find the testcase name in xls
	public static String getTestCaseName(String sTestCase) throws Exception {
		String value = sTestCase;
		try {
			int posi = value.indexOf("@");
			value = value.substring(0, posi);
			posi = value.lastIndexOf(".");
			value = value.substring(posi + 1);
			return value;
		} catch (Exception e) {
			logger.info(e.toString());
			throw (e);
		}
	}

	// method to find the testdata corresponding to testcase
	public static int getRowContains(String sTestCaseName, int colNum) throws Exception {
		int i;
		try {
			int rowCount = CommonUtils.getRowUsed();
			for (i = 0; i < rowCount; i++) {
				if (CommonUtils.getCellData(i, colNum).equalsIgnoreCase(sTestCaseName)) {
					break;
				}
			}
			return i;
		} catch (Exception e) {
			logger.info(e.toString());
			throw (e);
		}
	}

	// method to find the row used in the testcase
	public static int getRowUsed() throws Exception {
		try {
			int RowCount = ExcelWSheet.getLastRowNum();
			return RowCount;
		} catch (Exception e) {
			logger.info(e.toString());
			throw (e);
		}
	}

	// Method to click an element using Space
	public static void waitFindClickUsingSpace(By by) {
		try {
			waitForElement(by);
			driver.findElement(by).sendKeys(Keys.SPACE);
			logger.info("The object has been clicked successfully" + by.toString());
		} catch (Exception e) {
			logger.error(e); // should stop the execution in case of logger failure
			assert (false);
		}
	}

	// Method to wait for an element (explicit wait, the object time out is provided
	// in the config.properties file)
	public static boolean waitForElement(By by) {
		try {
			int objectTimeOutSeconds = Integer.parseInt(props.getProperty("objectTimeout")); // Wait Time flows from
																								// config.properties
			WebDriverWait waitDriver = new WebDriverWait(driver, objectTimeOutSeconds);
			waitDriver.until(ExpectedConditions.presenceOfElementLocated(by));
			logger.info("Waited success for " + by.toString());
			return true;
		} catch (WebDriverException e) {
			if (checkPresenceOfAlert()) {
				clickAlertPopup();
			} else {
				logger.error(e);
				Assert.fail();
				assert (false);
			}
			return false;
		}
	}

	// Method to find presence of element
	public static boolean checkElementPresence(By by) {
		try {
			if (driver.findElements(by).size() != 0) {
				// logger.info("The element is present "+by.toString());
				return true;
			} else {
				logger.info("The element not present " + by.toString());
			}

		} catch (Exception e) {
			logger.error(e);
			Assert.fail();
		}
		return false;
	}

	// Method to verify that file has been downloaded yet
	public static boolean checkFileDownload(String fileName) throws InterruptedException {
		boolean fileDownloaded = false;
		try {
			int retryCount = 0;
			while (!(fileDownloaded = isFileDownloaded(fileName))
					&& retryCount++ < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				logger.info("The file has not been downloaded yet");
				Thread.sleep(500);
			}
		} catch (Exception e) {
			Assert.fail();
			logger.error("The error is", e);
		}
		return fileDownloaded;
	}

	// Check the file in download folder and check the size of the file
	private static boolean isFileDownloaded(String fileName) {

		logger.info("File Path : " + outputFolder);
		File directory = new File(outputFolder);
		File[] directoryContents = directory.listFiles();
		logger.info("The method will now find the files matching the file name");
		for (File file : directoryContents) {
			logger.info("Name of the file in the folder : " + file.getName());
			logger.info("Name of the file Passed in the method : " + fileName);
			if (file.getName().equalsIgnoreCase(fileName)) {
				logger.info("The file " + fileName + " is present in the downloads folder");
				if (file.length() == 0 || file == null) {
					logger.info("The file " + fileName + " is zero size");
					assert false;
				}
				return true;
			}

		}
		return false;
	}

	// Method to check if element is displayed or not
	public static boolean checkElementdisplayed(By by) {
		try {
			if (driver.findElement(by).isDisplayed()) {
				logger.info("The element is displayed " + by.toString());
				return true;
			} else {
				logger.info("The element is not displayed " + by.toString());
			}
		} catch (Exception e) {
			logger.error(e);
			Assert.fail();
		}
		return false;
	}

	// Method to check if element is displayed or not
	public static boolean checkElementEnabled(By by) {
		try {
			if (driver.findElement(by).isEnabled()) {
				logger.info("The element is displayed " + by.toString());
				return true;
			} else {
				logger.info("The element is not displayed " + by.toString());
			}
		} catch (Exception e) {
			logger.error(e);
			Assert.fail();
		}
		return false;
	}

	// Method to check if element text is displayed or not
	public static boolean isTextPresent(String text) {
		try {
			boolean b = driver.getPageSource().contains(text);
			return b;
		} catch (Exception e) {
			return false;
		}
	}

	// Method to check if alert message is displayed when launching the browser
	public static boolean checkPresenceOfAlert() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	// Method to wait for the page load (explicit wait, the Page time out is
	// provided in the config.properties file)
	public static void waitForPageLoad() {
		try {
			int pageTimeOutSeconds = Integer.parseInt(props.getProperty("pageTimeOut")); // Wait time flows from
																							// config.properties
			driver.manage().timeouts().pageLoadTimeout(pageTimeOutSeconds, TimeUnit.SECONDS);
			logger.info("Page loaded successfully " + driver.getTitle());
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method to wait and enter text in a text field
	public void waitFindEnterText(By by, String text) {
		try {
			waitForElement(by);
			driver.findElement(by).clear();
			driver.findElement(by).sendKeys(text);
			;
			logger.info("Object found success " + by.toString() + " and entered the value " + text);
			waitForPageLoad();
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method to f attachments

	public void selectFile(By by, String filePath) throws AWTException {
		Robot robot = new Robot();
		StringSelection stringSelection = new StringSelection(filePath);

		try {
			if (getBrowserDetails().equalsIgnoreCase("firefox")) {
				javaScriptClickUsingBy(by);
			}
			if (getBrowserDetails().equalsIgnoreCase("chrome")) {
				waitFindClick(by);
				Thread.sleep(1000);
			}
			robot.setAutoDelay(200);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			// native key strokes for CTRL, V and ENTER keys
			robot.setAutoDelay(200);
			// Open Goto window
			robot.keyPress(KeyEvent.VK_META);
			robot.keyPress(KeyEvent.VK_SHIFT);
			robot.keyPress(KeyEvent.VK_G);
			robot.keyRelease(KeyEvent.VK_META);
			robot.keyRelease(KeyEvent.VK_SHIFT);
			robot.keyRelease(KeyEvent.VK_G);
			// Paste the clipboard value
			robot.keyPress(KeyEvent.VK_META);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_META);
			robot.keyRelease(KeyEvent.VK_V);

			// Press Enter key to close the Goto window and Upload window
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		} finally {
			robot = null;
			stringSelection = null;
		}

	}

	// Select file with move to element
	public void selectFile2(By by, String filePath) throws AWTException {
		Robot robot = new Robot();
		StringSelection stringSelection = new StringSelection(filePath);

		try {
			if (getBrowserDetails().equalsIgnoreCase("firefox")) {
				javaScriptClickUsingBy(by);
			}
			if (getBrowserDetails().equalsIgnoreCase("chrome")) {
				// javaScriptClickUsingBy(by);
				moveToWebElementAndActionClick(by);
				Thread.sleep(2000);
			}
			robot.setAutoDelay(200);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSelection, null);
			// native key strokes for CTRL, V and ENTER keys
			robot.setAutoDelay(200);
			// Open Goto window
			robot.keyPress(KeyEvent.VK_META);
			robot.keyPress(KeyEvent.VK_SHIFT);
			robot.keyPress(KeyEvent.VK_G);
			robot.keyRelease(KeyEvent.VK_META);
			robot.keyRelease(KeyEvent.VK_SHIFT);
			robot.keyRelease(KeyEvent.VK_G);
			// Paste the clipboard value
			robot.keyPress(KeyEvent.VK_META);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_META);
			robot.keyRelease(KeyEvent.VK_V);

			// Press Enter key to close the Goto window and Upload window
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		} finally {
			robot = null;
			stringSelection = null;
		}

	}

	// Uploading file

	public void uploadFile(By by, String filePath) throws Exception {
		File fileToUpload = new File(filePath);
		driver.findElement(by).sendKeys(fileToUpload.getCanonicalPath());
		Thread.sleep(2000);
	}

	// force stop
	public void hardStop(long second) throws Exception {
		Thread.sleep(second);
	}

	// method to upload the file
	public static void navigateFile() throws AWTException {
		Robot robot2 = new Robot();
		try {
			robot2.setAutoDelay(500);
			robot2.keyPress(KeyEvent.VK_ENTER);
			robot2.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		} finally {
			robot2 = null;
		}

	}

	// method to upload the file
	public static void addFile() {
		try {

			Robot robot1 = new Robot();
			robot1.setAutoDelay(500);
			robot1.keyPress(KeyEvent.VK_ENTER);
			robot1.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}

	}

	// Method to wait and click a button
	public static void waitFindClick(By by) {
		try {
			waitForElement(by);
			if (driver.findElement(by).isEnabled() == true) {
				javaScriptScrollUsingBy(by);
				driver.findElement(by).click();
				waitForPageLoad();
				logger.info("Object clicked successfully" + by.toString());
			} else {
				logger.info("Object is not enabled" + by.toString());
				assert (false);
			}
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	public synchronized void clickOnNameFromList(By by, String text) throws Exception {
		// By.cssSelector("div.xS > div > div.y6 > span"));
		List<WebElement> emailList = driver.findElements(by);
		for (int i = 0; i < emailList.size(); i++) {
			System.out.println(emailList.get(i).getText());
			Thread.sleep(1000);
			if (emailList.get(i).getText().equals(text) == true) {
				emailList.get(i).click();
				System.out.println("Value selected from list : " + emailList.get(i).getText());
				break;
			}
		}
	}

	// method to click checkbox
	public static void waitFindClickCheckBox(By by) {
		try {
			waitForElement(by);
			if (driver.findElement(by).isSelected() == false) {
				javaScriptScrollUsingBy(by);
				driver.findElement(by).click();
				waitForPageLoad();
				logger.info("Checkbox Object clicked successfully" + by.toString());
			}
			waitForPageLoad();
			logger.info("Object clicked successfully" + by.toString());
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// method to wait and click a link using webelement
	public static void findClick(WebElement by) {
		try {
			Actions actions = new Actions(driver);
			actions.moveToElement(by).click().build().perform();
			waitForPageLoad();
			logger.info("Object clicked successfully " + by.toString());
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}

	}

	// Method to move to an element and click
	public static void moveToWebElementAndActionClick(By by) {
		try {
			waitForElement(by);
			javaScriptScrollUsingBy(by);
			Actions actions = new Actions(driver);
			WebElement element = driver.findElement(by);
			actions.moveToElement(element).click().build().perform();
			logger.info("The object was clicked successfuly");
		} catch (Exception e) {
			logger.error(e); // should stop the execution in case of logger failure
			assert (false);
		}
	}

	// Method to click on Hover and do next click
	public static void longPressHoverClick(By by, By bie) {
		try {
			WebElement element = driver.findElement(by);
			Actions action = new Actions(driver);
			action.moveToElement(element).perform();
			WebElement subElement = driver.findElement(bie);
			action.moveToElement(subElement);
			action.click();
			action.perform();
			
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}

	}

	// Method to select an option from dropdown
	public static void waitFindSelectDropDown(By by, String option) {
		try {
			waitForElement(by);
			Select dropdown = new Select(driver.findElement(by));
			dropdown.selectByVisibleText(option);
			logger.info("Object selected " + option + " successfully" + by.toString());

		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method to retry select an option from dropdown
	public static void retryFindSelect(By by, String option) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 2) {
			try {
				waitForElement(by);
				javaScriptScrollUsingBy(by);
				Select dropdown = new Select(driver.findElement(by));
				dropdown.selectByVisibleText(option);
				logger.info("Object selected " + option + " successfully" + by.toString());
				result = true;
				break;
			} catch (Exception e) {
				logger.error(e);
			}
			attempts++;
		}
	}

	// Method to enter data in text field incase ajax actions are required
	public void waitFindAKeyBoardEnter(By by, String text) {
		Actions mouse = new Actions(driver);
		WebElement element = driver.findElement(by);
		mouse.moveToElement(element).click().sendKeys(Keys.HOME,Keys.chord(Keys.SHIFT,Keys.END),text).build().perform();
	}

	// Method to close all windos
	public static void closeWindows() {
		driver.close();
	}

	// Method to close current window
	public static void closeCurrentWindow() {
		try {
			Set<String> ids = driver.getWindowHandles();
			Iterator<String> it = ids.iterator();
			String parentId = it.next();
			String childId = it.next();
			driver.switchTo().window(childId);
			driver.close();
			driver.switchTo().window(parentId);
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method to close specific window
	public void closeSpecificWindow(String windowTitle) {
		Set<String> windows = driver.getWindowHandles();
		String mainwindow = driver.getWindowHandle();
		for (String handle : windows) {
			driver.switchTo().window(handle);
			System.out.println("switched to " + driver.getTitle() + "  Window");
			String pagetitle = driver.getTitle();
			if (pagetitle.equalsIgnoreCase(windowTitle)) {
				driver.close();
				System.out.println("Closed the  '" + pagetitle + "' Tab now ...");
			}
		}
		driver.switchTo().window(mainwindow);
	}

	// Method to wait for dynamic load. //parameter1 = id of the spinner and
	// parameter2 = expected class attribute value which changes instantly

	public static void waitForDynamicLoad(By by, String objectClassName) {
		try {
			int MaxTimeWait = Integer.parseInt(props.getProperty("pageTimeOut"));
			Instant current;
			int _pollTimer = 500; // millisec
			Instant start = Instant.now();
			Thread.sleep(_pollTimer);
			logger.info("checking if page/data is being refreshed.....");
			WebElement object = driver.findElement(by);
			String objectClass = object.getAttribute("class");
			while (!objectClassName.equalsIgnoreCase(objectClass)) {
				Thread.sleep(_pollTimer);
				object = driver.findElement(by);
				objectClass = object.getAttribute("class");
				logger.info("waiting for obj " + by.toString());
				current = Instant.now();
				if (Duration.between(start, current).toMillis() >= MaxTimeWait * 1000) {
					logger.info("Wait Logo Timeout after " + MaxTimeWait + " seconds");
					throw new InterruptedException(
							"Waited more than " + MaxTimeWait + " seconds and still Data Loading on Page");
				}
			}
			logger.info(".....page/data refresh is now complete");
			Thread.sleep(_pollTimer);
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	//

	// Method to compare 2 strings
	public static boolean assertString(String expectedObject, String actualObject) {
		try {
			Assert.assertEquals(expectedObject, actualObject);
			logger.info("Expected " + expectedObject + " and actual " + actualObject + " matched");
			return (true);
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
		return false;
	}

	// Method to compare the 2 strings
	public static void assertContainsString(String expectedObject, String actualObject) {
		try {
			if (actualObject.toLowerCase().contains(expectedObject.toLowerCase())) {
				assert (true);
				logger.info("Expected " + expectedObject + " and actual " + actualObject + " matched");
			} else {
				logger.info("Expected " + expectedObject + " and actual " + actualObject + " did not match");
				assert (false);
			}
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method to get the text of the object from application
	public static String textFromApplication(By by) {
		boolean result = false;
		int attempts = 0;
		String text = null;
		while (attempts < 2) {
			try {
				waitForElement(by);
				text = driver.findElement(by).getText();
				logger.info("The text from the object is " + text);
				result = true;
				break;
			} catch (Exception e) {
				logger.error(e);
			}
			attempts++;
		}
		return text;
	}

	// Method to get the text of the object from application and covert text to
	// double
	public static Integer textFromApplicationAndConvertToInteger(By by) {
		boolean result = false;
		int attempts = 0;
		String text = null;
		Integer intgr = null;
		while (attempts < 2) {
			try {
				waitForElement(by);
				text = driver.findElement(by).getText();
				intgr = Integer.parseInt(text);
				logger.info("The text from the object is " + text);
				result = true;
				break;
			} catch (Exception e) {
				logger.error(e);
			}
			attempts++;
		}
		return intgr;
	}

	// Method to get the text of the object from application and covert text to
	// double
	public static Double textFromApplicationAndConvertToDouble(By by) {
		boolean result = false;
		int attempts = 0;
		String text = null;
		Double duble = null;
		while (attempts < 2) {
			try {
				waitForElement(by);
				text = driver.findElement(by).getText();
				duble = Double.parseDouble(text);
				logger.info("The text from the object is " + text);
				result = true;
				break;
			} catch (Exception e) {
				logger.error(e);
			}
			attempts++;
		}
		return duble;
	}

	// Method to get the text of the object from object
	public static String textFromObject(By by) {
		boolean result = false;
		int attempts = 0;
		String text = null;
		while (attempts < 2) {
			try {
				waitForElement(by);
				text = driver.findElement(by).getAttribute("value");
				logger.info("The text from the object is " + text);
				result = true;
				break;
			} catch (Exception e) {
				logger.error(e);
			}
			attempts++;
		}
		return text;
	}

	// Method to Click using javascript
	public static void javaScriptClickUsingBy(By by) {
		try {
			// waitForElement(by);
			WebElement element = driver.findElement(by);
			if (element.isEnabled()) {
				javaScriptScrollUsingBy(by);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				logger.info("The object " + by.toString() + " is clicked successfully using javascript");
			} else {
				logger.info("The object " + by.toString() + " is is not enabled to be clicked using javascript");
			}

		} catch (Exception e) {
			logger.error(e);
		}
	}

	// Method to scroll to particular element
	public static void javaScriptScrollUsingBy(By by) {
		try {
			waitForElement(by);
			WebElement element = driver.findElement(by);
			if (checkElementdisplayed(by)) {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].scrollIntoView();", element);
				logger.info("The object " + by.toString() + " is clicked successfully using javascript");
			} else {
				logger.info("The object " + by.toString() + " is is not enabled to be clicked using javascript");
			}

		} catch (Exception e) {
			logger.error(e);
		}
	}

	// Setting Field type for process fields
	static enum FieldType {
		STRING, NUMBER, EMAIL
	}

	// method to processfields based on type, length
	public void processFields(FieldType type, int length, By field, String name) {
		try {
			String generatedString = null;
			switch (type) {
			case STRING:
				generatedString = generateString(length);
				break;
			case NUMBER:
				generatedString = generateNumbers(length);
				break;
			case EMAIL:
				generatedString = generateString(length) + "@gmail.com";
				break;
			}
			waitFindEnterText(field, generatedString);
			logger.info("The " + name + " is " + generatedString);
		} catch (Exception e) {
			logger.error(e);
		}

	}

	// method to get browser
	public static String getBrowserDetails() {
		try {
			Capabilities caps = ((RemoteWebDriver) driver).getCapabilities();
			String browserName = caps.getBrowserName();
			logger.info("The browser detail is " + browserName);
			return browserName;
		} catch (Exception e) {
			logger.error(e);
		}
		return null;

	}

	// method to enter text using javascript
	public static void javaScriptEnterText(By by, String text) {
		try {
			waitForElement(by);
			javaScriptScrollUsingBy(by);
			if (driver.findElement(by).isEnabled()) {
				WebElement element = driver.findElement(by);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].enabled = true", element);
				executor.executeScript("arguments[0].setAttribute('value', '" + text + "')", element);
				logger.info("The " + text + " has been entered successfully on the object " + by.toString());
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	// Method to Click using javascript
	public static void javaScriptCheckBox(By by) {
		try {
			waitForElement(by);
			javaScriptScrollUsingBy(by);
			if (!driver.findElement(by).isSelected() && driver.findElement(by).isEnabled()) {
				WebElement element = driver.findElement(by);
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", element);
				logger.info("The object " + by.toString() + " is clicked successfully using javascript");
			} else if (!driver.findElement(by).isEnabled()) {
				logger.info("The object " + by.toString() + " is not enabled");
			} else {
				logger.info("The object " + by.toString() + " is already checked");
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	// Method to Click using javascript
	public static boolean javaScriptClickUsingWebElement(WebElement by) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 2) {
			try {
				JavascriptExecutor executor = (JavascriptExecutor) driver;
				executor.executeScript("arguments[0].click();", by);
				result = true;
				logger.info("The object " + by.toString() + " is clicked successfully using javascript");
				break;
			} catch (StaleElementReferenceException e) {
				logger.error(e);
			}
			attempts++;
		}
		return result;
	}

	// Method to retry find and click in case of stale element exception
	public boolean retryingFindClick(By by) {
		boolean result = false;
		int attempts = 0;
		while (attempts < 2) {
			try {
				driver.findElement(by).click();
				result = true;
				break;
			} catch (StaleElementReferenceException e) {
			}
			attempts++;
		}
		return result;
	}

	// Method to generate random text
	public static String generateString(int length) {
		Random rng = new Random();
		String characters = "abcdefghijklmnopqrstuvwxyz";
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	// Method to generate random numbers
	public static String generateNumbers(int length) {
		Random rng = new Random();
		String characters = "1234567890";
		char[] text = new char[length];
		for (int i = 0; i < length; i++) {
			text[i] = characters.charAt(rng.nextInt(characters.length()));
		}
		return new String(text);
	}

	// Method to find Elements inside the frame
	public static void findElementsInsideFrame(By by) {
		try {
			driver.switchTo().frame(0);
			logger.info("in frame");
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method switch to inside the frame
	public static void switchInsideFrame(By by) {
		try {
			int frameIndex = 0;
			List<WebElement> listFrames = driver.findElements(by);
			System.out.println("list frames   " + listFrames.size());
			driver.switchTo().frame(listFrames.get(frameIndex));
			logger.info("in frame");
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method to move out of frame
	public static void moveOutFrame() {
		try {
			driver.switchTo().defaultContent();
			System.out.println("out frame");
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method to click popup
	public static void clickAlertPopup() {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (NoAlertPresentException e) {
			logger.error(e);
			assert (false);
		}
	}

	// Method to click popup
	public static void closeAlertPopup() {
		try {
			Robot robot = new Robot();
			robot.setAutoDelay(500);
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Create method to find the details of the table
	public void waitFindNumberOfTables(By by) {
		try {
			waitForElement(by);
			driver.findElements(by).size();
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	// Generating the key to Encrypt the password
	private static Key generateKey() {
		try {
			return new SecretKeySpec(keyValue, ALGO);
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
		return null;

	}

	// Method to Encrypt the password
	public static String encrypt(String passwordStr) {
		String encryptedValue = null;
		try {
			Key key = generateKey();
			Cipher chiper = Cipher.getInstance(ALGO);
			chiper.init(Cipher.ENCRYPT_MODE, key);
			byte[] encVal = chiper.doFinal(passwordStr.getBytes(Charset.defaultCharset()));
			encryptedValue = new String(Base64.encodeBase64(encVal), Charset.defaultCharset());
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
		return encryptedValue;
	}

	// Decrypting the password
	public static String decrypt(String encryptedStr) {
		String decryptedValue = null;
		try {
			Key key = generateKey();
			Cipher chiper = Cipher.getInstance(ALGO);
			chiper.init(Cipher.DECRYPT_MODE, key);
			byte[] decordedValue = Base64.decodeBase64(encryptedStr.getBytes(Charset.defaultCharset()));
			byte[] decValue = chiper.doFinal(decordedValue);
			decryptedValue = new String(decValue, Charset.defaultCharset());
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
		return decryptedValue;
	}

	// Method to find data in table
	public String findDataInTable(By by) {
		try {
			waitForElement(by);
			WebElement table = driver.findElement(by);
			List<WebElement> row = table.findElements(By.tagName("tr"));
			String actualtableContent = "";
			for (WebElement r : row) {
				List<WebElement> column = r.findElements(By.tagName("td"));
				for (WebElement c : column) {
					actualtableContent = actualtableContent + " " + c.getText();
				}
			}
			return actualtableContent;
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
		return null;
	}

	// Method to click the checkbox
	public void clickCheckbox(By by) {
		try {
			waitForElement(by);
			if (!driver.findElement(by).isSelected()) {
				driver.findElement(by).click();
			}
		} catch (Exception e) {
			logger.error(e);
		}
	}

	public static void loadApplicationPageUrl(String url, String title) {
		try {
			// Calling the url through the driver object
			driver.get(url);
			waitForPageLoad();
			// Setting time out if the credentials fails
			Assert.assertEquals(title, driver.getTitle());
		} catch (Exception e) {
			logger.error(e);
		}

	}

	// Remove the last character in a string text
	public String removeLastCharacter(String str) {
		try {
			if (str != null && str.length() > 0) {
				str = str.substring(0, str.length() - 1);
			}
			return str;
		} catch (Exception e) {
			logger.error(e);
		}
		return null;

	}

	// Make database connection using mysql
	public static String getRequestNumberFromDb(String applicationNumber) {
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			Connection con = DriverManager.getConnection(
					"jdbc:oracle:thin:@" + props.getProperty("dbHost") + ":" + props.getProperty("dbPort") + ":"
							+ props.getProperty("dataBaseName"),
					props.getProperty("dbUserName"), props.getProperty("dbPassWord"));
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(
					"SELECT request_id FROM WEB_APP_FRM_DETAILS where app_form_id in (SELECT app_form_id FROM WEB_REQUEST_DETAILS where  loraxid ="
							+ applicationNumber + ")");
			while (rs.next()) {
				logger.info("The Request ID is " + rs.getString("REQUEST_ID"));
				return (rs.getString("REQUEST_ID"));
			}
		} catch (Exception e) {
			logger.error(e);
		}
		return null;
	}

	// Method to Replacing all the text
	public static String replaceText(String text, String oldValue, String newValue) {
		try {
			text = text.replaceAll(oldValue, newValue);
			return text;
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
		return null;
	}

	// Method to refresh the page
	public static void refreshPage() {
		try {
			driver.navigate().refresh();
			waitForPageLoad();
		} catch (Exception e) {
			logger.error(e);
			assert (false);
		}
	}

	public void processFieldCommon(String fieldType, String fieldName, By by, String value,
			HashMap<String, String> map) {
		try {
			if (fieldType.equalsIgnoreCase("TextField")) {
				waitFindEnterText(by, value);

			} else if (fieldType.equalsIgnoreCase("DropDown")) {
				waitFindSelectDropDown(by, value);
			} else if (fieldType.equalsIgnoreCase("retryDropDown")) {
				retryFindSelect(by, value);
			} else if (fieldType.equalsIgnoreCase("RadioButton")) {
				javaScriptClickUsingBy(by);
			}
			map.put(fieldName, value);
		} catch (Exception e) {
			logger.error(e);
		}
	}

	// methods
	public static Boolean accessbutton(By by) {
		try {
			waitForElement(by);
			javaScriptScrollUsingBy(by);
			javaScriptClickUsingBy(by);
			return true;
		} catch (Exception e) {
			System.out.println(e);
			assert false;
			return false;
		}
	}

	public synchronized String getText(By by) throws Exception {
		String text = driver.findElement(by).getText();
		System.out.println(text);
		return text;
	}

	public static Boolean verifyTitle(String pageTitles) {
		try {
			Assert.assertEquals(driver.getTitle(), pageTitles);
			return true;
		} catch (Exception e) {
			System.out.println(e);
			assert false;
			return false;
		}
	}
}
