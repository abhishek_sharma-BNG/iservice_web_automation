package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class TrueMultipleSim extends CommonUtils {
	
	static final Logger LOGGER = Logger.getLogger(TrueMultipleSim.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By postpaid = By.xpath("//*[@id=\"bills_usage_billing_summary\"]/span");
	By chevronPostpaid = By.id("postpaid_0_chevron");	
	By multipleSim = By.id("bills_usage_postpaid_current_usage_0_multi_sim");
	By sim1 = By.id("bills_usage_multi_sim_0");
	By sim2 = By.id("bills_usage_multi_sim_1");
	By crossSim = By.className("_3qbUG8eHe4A5rBOLYLKoUV");

	
	// Method to verify multiple sim in postpaid account
	public void multipleSimPostpaidAccount() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(1000);
				retryCount++;
			}
		}
		if (textFromApplication(postpaid).equalsIgnoreCase("Billing Summary")) {
			logger.info("Postpaid section is selected");
			assert true;
		} else {
			logger.info("Postpaid section is not selected");
			assert false;
		}
		/*// click on chevron button
		Thread.sleep(2000);
		if (accessbutton(chevronPostpaid)) {
			assert true;
		} else {
			assert false;
		}*/
		Thread.sleep(1000);
		// click on Multiple Sim(2)
		if (accessbutton(multipleSim)) {
			if (checkElementPresence(loaderSpinner)) {
				int retryCount = 0;
				while (checkElementPresence(loaderSpinner) == true
						&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
					Thread.sleep(750);
					retryCount++;
				}
			}
			checkElementPresence(sim1);
			//checkElementPresence(sim2);
			waitFindClick(crossSim);
			assert true;
		}else {
			assert false;
		}
	}
}
