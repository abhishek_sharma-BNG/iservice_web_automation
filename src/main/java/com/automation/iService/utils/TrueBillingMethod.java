package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;

public class TrueBillingMethod extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueBillingMethod.class.getName());

	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By hamburger = By.id("header_nav_bar_hamburger");
	By accountSetting = By.id("header_side_bar_0_1_trigger_label");
	By billingMethod = By.id("header_side_bar_WEB__ACCOUNT_BILLING_0");
	By editEbillButton = By.xpath("//*[@id=\"billingPreferenceId_eCard_click_value_1\"]/div/img");
	By eamilOrSMS = By.id("billingPreferenceId_eCard_address_Label_1");
	By emailField = By.id("billing_method_edit_e_bill_email_textbox_field");
	By saveButton = By.id("billing_method_edit_e_bill_save_button");
	By confirmButton = By.id("confirm_button");
	By emailSMSText = By.id("billingPreferenceId_eCard_address_value_1");
	By smsField = By.id("billing_method_edit_e_bill_sms_textbox_field");
	By toggleButton1 = By.id("e_bill_type_sms_togglebtn_span");
	By toggleButton2 = By.id("ebill_mail_togglebtn_span");

	// Method to verify edit bill
	public void verifyEditEBill() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(750);
				retryCount++;
			}
		}

		try {
			boolean button = true;
			while (button) {
				longPressHoverClick(hamburger, accountSetting);
				javaScriptScrollUsingBy(billingMethod);
				moveToWebElementAndActionClick(billingMethod);
				boolean checkPresence = driver.findElements(eamilOrSMS).size() != 0;
				if (checkPresence) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(750);
				retryCount++;
			}
		}
		hardStop(2000);
		if (checkElementPresence(eamilOrSMS)) {
			if (textFromApplication(eamilOrSMS).equalsIgnoreCase("EMAIL :")) {
				javaScriptScrollUsingBy(editEbillButton);
				waitFindClick(editEbillButton);
				javaScriptScrollUsingBy(emailField);
				waitFindAKeyBoardEnter(emailField, TestData.EMAIL);
				javaScriptScrollUsingBy(saveButton);
				moveToWebElementAndActionClick(saveButton);
				javaScriptScrollUsingBy(confirmButton);
				moveToWebElementAndActionClick(confirmButton);
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(750);
						retryCount++;
					}
				}
				driver.navigate().refresh();
				assertContainsString(textFromApplication(emailSMSText), TestData.EMAIL);
			} else if (textFromApplication(eamilOrSMS).equalsIgnoreCase("SMS :")) {
				javaScriptScrollUsingBy(editEbillButton);
				waitFindClick(editEbillButton);
				javaScriptScrollUsingBy(smsField);
				waitFindAKeyBoardEnter(smsField, TestData.SMS);
				javaScriptScrollUsingBy(saveButton);
				moveToWebElementAndActionClick(saveButton);
				javaScriptScrollUsingBy(confirmButton);
				moveToWebElementAndActionClick(confirmButton);
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(750);
						retryCount++;
					}
				}
				driver.navigate().refresh();
				assertContainsString(textFromApplication(emailSMSText), TestData.SMS);
			} else {
				Assert.fail();
				logger.info("Not able to edit the E-bill");
				assert (false);
			}
			if (textFromApplication(eamilOrSMS).equalsIgnoreCase("EMAIL :")) {
				javaScriptScrollUsingBy(editEbillButton);
				waitFindClick(editEbillButton);
				javaScriptScrollUsingBy(toggleButton1);
				moveToWebElementAndActionClick(toggleButton1);
				javaScriptScrollUsingBy(smsField);
				waitFindAKeyBoardEnter(smsField, TestData.SMS);
				javaScriptScrollUsingBy(saveButton);
				moveToWebElementAndActionClick(saveButton);
				javaScriptScrollUsingBy(confirmButton);
				moveToWebElementAndActionClick(confirmButton);
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(750);
						retryCount++;
					}
				}
				driver.navigate().refresh();
				assertContainsString(textFromApplication(emailSMSText), TestData.SMS);
			} else if (textFromApplication(eamilOrSMS).equalsIgnoreCase("SMS :")) {
				javaScriptScrollUsingBy(editEbillButton);
				waitFindClick(editEbillButton);
				javaScriptScrollUsingBy(toggleButton2);
				moveToWebElementAndActionClick(toggleButton2);
				javaScriptScrollUsingBy(emailField);
				waitFindAKeyBoardEnter(emailField, TestData.EMAIL);
				javaScriptScrollUsingBy(saveButton);
				moveToWebElementAndActionClick(saveButton);
				javaScriptScrollUsingBy(confirmButton);
				moveToWebElementAndActionClick(confirmButton);
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(750);
						retryCount++;
					}
				}
				driver.navigate().refresh();
				assertContainsString(textFromApplication(emailSMSText), TestData.EMAIL);
			} else {
				Assert.fail();
				logger.info("Not able to edit the E-bill");
				assert (false);
			}
		} else {
			Assert.fail();
			logger.info("Not able to edit the E-bill");
			assert (false);
		}
	}
}
