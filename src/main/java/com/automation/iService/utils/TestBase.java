package com.automation.iService.utils;

import java.io.File;
import java.io.FileNotFoundException;
//Importing the java utilities to read the properties file
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
//Importing log4j
import org.apache.log4j.Logger;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
//Importing the selenium webdriver related libraries
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;


//Creating a Test base class
public class TestBase {

	// Create objects for Extent reports
	ExtentReports extent;
	ExtentTest test;
	// Logger variable
	static final Logger logger = Logger.getLogger(TestBase.class.getName());

	// Variables for deriving testcases from xls
	private String sTestCaseName;
	private int iTestCaseRow;

	// Variable for recording the video
	private ATUTestRecorder recorder;

	// setting a variable as null
	public static WebDriver driver = null;

	// Declaring an object properties for the java class properties.
	// This props will hold the details in the properties file
	static ClassLoader loader = Thread.currentThread().getContextClassLoader();
	static Properties props;
	public static String browser;
	// Create a setup method to run before start of every suite

	public void setup(String browser) throws IOException 
	{
		this.browser = browser;
		try {
			// Read the property file
			readPropFile();

			/*
			 * if (browser.equalsIgnoreCase("safari")){ extent = new
			 * ExtentReports(System.getProperty("user.dir")+
			 * "/test-output/gmaccTestResult/gmaccWebAutomationreport"+dateFormat.format(
			 * date).toUpperCase()+".html", true); extent.addSystemInfo("Browser", browser);
			 * }
			 */
		
			FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/test-output/TrueTestResult"));
			if (browser.equalsIgnoreCase("chrome")) {
				extent = new ExtentReports(
						System.getProperty("user.dir") + "/test-output/TrueTestResult/TrueWebAutomationreport.html",
						false);
				extent.addSystemInfo("Browser", browser);
			} else if (browser.equalsIgnoreCase("firefox")) {
				extent = new ExtentReports(
						System.getProperty("user.dir") + "/test-output/TrueTestResult/TrueWebAutomationreport.html",
						false);
				extent.addSystemInfo("Browser", browser);
			}
			// Extent reports Setup;
			extent.addSystemInfo("Environment", "Cloud");
			extent.addSystemInfo("Team", "TrueiService-Dev Testing");
			extent.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));

			// test setup based on the details in the property file
			// Run the browser based on user configuration and bring focus
			
			if (browser.equalsIgnoreCase("chrome")) 
			{
				DesiredCapabilities caps = DesiredCapabilities.chrome();
				File file = new File(props.getProperty("Pathchromedriver"));
				System.setProperty(props.getProperty("chromedriver"), file.getAbsolutePath());
				ChromeOptions options = new ChromeOptions();
				options.addArguments("start-maximized");
				options.addArguments("--disable-impl-side-painting");		
				caps.setCapability("platform", "Windows 7");
				caps.setCapability("version", "38.0");
				driver = new ChromeDriver(options);	
				//driver = new RemoteWebDriver(new URL("http://sharma2908abhishek:dcd3f9e8-8130-4e8c-8dbe-898cb85aa4a8@ondemand.saucelabs.com:80/wd/hub"), caps);
				getDriver.setDriver(driver);
			
			
			} else if (browser.equalsIgnoreCase("firefox"))
			{
				File file = new File(props.getProperty("pathGeckodriver"));
				System.setProperty(props.getProperty("Geckodriver"), file.getAbsolutePath());
				driver = new FirefoxDriver();
				getDriver.setDriver(driver);				
				driver.manage().window().maximize();
			}
			
			// Delete all previous videos and recordings
			FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/test-output/Recordings"));
			FileUtils.cleanDirectory(new File(System.getProperty("user.dir") + "/test-output/Screenshots"));
		} catch (Exception e) {
			logger.info(e.toString());
		}
	}

	// Before each test load the url
	public void loadUrl(String url, String siteTitle) {
		try {
			// Calling the url through the driver object
			driver.get(url);
			// Setting time out if the credentials fails
			Assert.assertEquals(siteTitle, driver.getTitle());
			driver.switchTo().window(driver.getTitle());
			JavascriptExecutor executor = (JavascriptExecutor) driver;
			executor.executeScript("window,focus()");
		} catch (Exception e) {
			logger.info(e.toString());
		}
	}
	
	// @BeforeMethod
	public void startRecording(String method) throws ATUTestRecorderException {

		try {
			this.recorder = new ATUTestRecorder(System.getProperty("user.dir") + "/test-output/Recordings/", method,
					false);
			recorder.start();
		} catch (Exception e) {
			logger.info(e.toString());
		}

	}

	// After every failure testcase, the method will take a screenshot
	@AfterMethod
	public void takescreenshotonfail(ITestResult result, Method method) throws IOException, ATUTestRecorderException {
		try {
			recorder.stop();
			renameFileExtension(System.getProperty("user.dir") + "/test-output/Recordings/" + method.getName() + ".mov",
					"mp4");
			String movie = test.addScreencast(
					System.getProperty("user.dir") + "/test-output/Recordings/" + method.getName() + ".mp4");

			if (ITestResult.SUCCESS == result.getStatus()) {
				// delete the file
				FileUtils.forceDelete(new File(
						System.getProperty("user.dir") + "/test-output/Recordings/" + method.getName() + ".mp4"));
				logger.info("The movie file deleted as the testcase passed");
			}

			// Here will compare if test is failing then only it capture the video and
			// provide the link
			else if (ITestResult.FAILURE == result.getStatus()) {
				// Call method to capture screenshot
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				// Copy files to specific location here it will save all screenshot in our project home directory and
				// result.getName() will return name of test case so that screenshot name will be same
				String screenshotPath = props.getProperty("screenshotsPath") + result.getName() + ".png";
				FileUtils.copyFile(scrFile, new File((screenshotPath)));
				logger.info("Screenshot taken : " + result.getName());
				// Add the screenshot to the report
				test.log(LogStatus.FAIL, result.getName(), movie);
				test.log(LogStatus.INFO, "Download the video file using right click -> save videos as option");
				logger.info("The link to screen recording is captured in the report");
				String image = test.addScreenCapture(
						System.getProperty("user.dir") + "/test-output/Screenshots/" + result.getName() + ".png");
				test.log(LogStatus.FAIL, result.getName(), image);
				// test.log(LogStatus.FAIL,result.getThrowable());
			}
			extent.endTest(test);
		} catch (Exception e) {
			logger.info(e.toString());
			extent.endTest(test);
		}
	}
	// After each and every testcase, close all windows
	@AfterTest
	public void closeAllWindows() {
		try {
			String homeWindow = driver.getWindowHandle();
			Set<String> allWindows = driver.getWindowHandles();

			// Use Iterator to iterate over windows
			Iterator<String> windowIterator = allWindows.iterator();

			// Verify next window is available
			while (windowIterator.hasNext()) {

				// Store the child window id
				String childWindow = windowIterator.next();

				if (homeWindow.equals(childWindow)) {
					driver.switchTo().window(childWindow);
					// driver.manage().deleteAllCookies(); //delete all cookies
					deleteAllCookiesOneByOne();

				}
			}

		} catch (Exception e) {
			logger.info(e.toString());
		}

	}

	@AfterSuite
	public void quitDrivers() {
		try {
			driver.quit();
			extent.flush();
			extent.close();
		} catch (Exception e) {
			logger.info(e.toString());
		}
	}

	// Method to load and read the property file
	public static void readPropFile() {
		logger.info("Reading properties file");
		props = new Properties();
		InputStream stream = loader.getResourceAsStream("config.properties");
		try {
			props.load(stream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@DataProvider
	public Object[][] TestData() throws Exception {
		// Setting up the Test Data Excel file
		CommonUtils.setExcelFile(System.getProperty("user.dir") + "/src/test/resources/testData.xlsx", "Sheet1");
		sTestCaseName = this.toString();
		// From above method we get long test case name including package and class name
		// etc.
		// The below method will refine your test case name, exactly the name use have
		// used
		sTestCaseName = CommonUtils.getTestCaseName(this.toString());
		// Fetching the Test Case row number from the Test Data Sheet
		// Getting the Test Case name to get the TestCase row from the Test Data Excel
		// sheet
		iTestCaseRow = CommonUtils.getRowContains(sTestCaseName, 0);
		Object[][] testObjArray = CommonUtils.getTableArray(
				System.getProperty("user.dir") + "/src/test/resources/testData.xlsx", "Sheet1", iTestCaseRow);
		return (testObjArray);
	}

	// To rename the extension of the file
	public static boolean renameFileExtension(String source, String newExtension)
			throws FileNotFoundException, NullPointerException {
		String target;
		String currentExtension = getFileExtension(source);

		if (currentExtension.equals("")) {
			target = source + "." + newExtension;
		} else {
			target = source.replaceFirst(Pattern.quote("." + currentExtension) + "$",
					Matcher.quoteReplacement("." + newExtension));
		}
		return new File(source).renameTo(new File(target));
	}

	// to get the file extension
	public static String getFileExtension(String sourceFile) {
		String ext = "";
		int i = sourceFile.lastIndexOf('.');
		if (i > 0 && i < sourceFile.length() - 1) {
			ext = sourceFile.substring(i + 1);
		}
		return ext;
	}

	// Method to delete all cookies
	public void deleteAllCookiesOneByOne() {
		try {
			int noOfCookies = driver.manage().getCookies().size();
			if (noOfCookies > 0) {
				logger.info("Number of cookies found: " + Integer.toString(noOfCookies));
			}
			Set<Cookie> cookies = driver.manage().getCookies();
			for (Cookie cookie : cookies) {
				logger.info("Cookie found with name: " + cookie.getName() + " and path: " + cookie.getPath()
						+ " and domain: " + cookie.getDomain());
				String javascriptCall = "document.cookie = \"" + cookie.getName() + " path=" + cookie.getPath()
						+ "; expires=Thu, 01-Jan-1970 00:00:01 GMT;\"";
				logger.info("Attempting to expire the cookie with the following script: " + javascriptCall);
				((JavascriptExecutor) driver).executeScript(javascriptCall);
			}
			logger.info("Number of cookies is now: " + Integer.toString(driver.manage().getCookies().size()));
		} catch (Exception e) {
			logger.info(e.toString());
		}
	}
}
