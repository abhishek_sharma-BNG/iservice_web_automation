package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TrueVision extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueVision.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By postpaid = By.xpath("//*[@id=\"bills_usage_billing_summary\"]/span");
	By trueVisionNumber = By.id("bills_usage_acc_card_product_value_tvs_0");
	By chevronButton = By.xpath("//*[@id=\"tvs_0_chevron\"]/i");
	By balance = By.id("checkbox_amount_tvs_0");
	By planName = By.id("tvs_0_price_plan");
	By currentUsage = By.id("bills_usage_vision_tab_view_0_first_label");
	By hdChannels = By.id("bills_usage_vision_usage_0_hd_channel");
	By channels = By.id("bills_usage_vision_usage_0_channel");
	By billDetail = By.id("bills_usage_vision_tab_view_0_second_label");
	By amount0 = By.id("checkbox_amount_bills_usage_vision_bill_detail_0-invoice-checkbox-0");
	By amount1 = By.id("checkbox_amount_bills_usage_vision_bill_detail_0-invoice-checkbox-1");
	By amount2 = By.id("checkbox_amount_bills_usage_vision_bill_detail_0-invoice-checkbox-2");
	By amount3 = By.id("checkbox_amount_bills_usage_vision_bill_detail_0-invoice-checkbox-3");
	By amount4 = By.id("checkbox_amount_bills_usage_vision_bill_detail_0-invoice-checkbox-4");
	By amount5 = By.id("checkbox_amount_bills_usage_vision_bill_detail_0-invoice-checkbox-5");
	By amount6 = By.id("checkbox_amount_bills_usage_vision_bill_detail_0-invoice-checkbox-6");

	By viewDetail = By.id("bills_usage_vision_bill_detail_0-invoice-month-0_view_details");
	By searchButton = By.id("tvs_0_search");
	By tueVisionNumberOnPopUp = By.id("popupPanel_productValue");
	By popPlanName = By.id("tvs_0_planName_Vision");
	By hdChannelsOnPopup = By.id("tvs_0_channel_count_hd");
	By channelsOnPopup = By.id("tvs_0_normal_channel_count");
	By crossPackage = By.id("popupPanel_closeIcon");

	String trueVisionNumberOnCard, planNameOnCard, hdChannelText, channelText;
	Double totalAmount;

	// Method to verify True vision account
	public void trueVisionAccount() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				hardStop(2000);
				retryCount++;
			}
		}

		if (textFromApplication(postpaid).equalsIgnoreCase("Billing Summary")) {
			logger.info("Postpaid section is selected");
			assert true;
		} else {
			logger.info("Postpaid section is not selected");
			assert false;
		}
		Thread.sleep(1000);
		if (checkElementPresence(trueVisionNumber)) {
			accessbutton(chevronButton);
			hardStop(2000);
			assert true;
		} else {
			assert false;
		}
		waitForElement(trueVisionNumber);
		trueVisionNumberOnCard = getText(trueVisionNumber);
		waitForElement(planName);
		planNameOnCard = getText(planName);
		if(checkElementPresence(balance)){
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(planName)){
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(currentUsage)){
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(hdChannels)){
			assert true;
		} else {
			assert false;
		}
		hdChannelText = getText(hdChannels);
		if (checkElementPresence(channels)){
			assert true;
		} else {
			assert false;
		}
		channelText = getText(channels);
		if (checkElementPresence(billDetail)){
			assert true;
		} else {
			assert false;
		}

		if (checkElementPresence(balance)) {
			String amount = getText(balance);
			String tAmount = amount.replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);
			String getText0 = getText(amount0);
			String amount0 = getText0.replaceAll(",", "");
			String getText1 = getText(amount1);
			String amount1 = getText1.replaceAll(",", "");
			String getText2 = getText(amount2);
			String amount2 = getText2.replaceAll(",", "");
			String getText3 = getText(amount3);
			String amount3 = getText3.replaceAll(",", "");
			String getText4 = getText(amount4);
			String amount4 = getText4.replaceAll(",", "");
			javaScriptScrollUsingBy(amount5);
			String getText5 = getText(amount5);
			String amount5 = getText5.replaceAll(",", "");
			double sumOfAmount = Double.parseDouble(amount0) + Double.parseDouble(amount1) + Double.parseDouble(amount2)
					+ Double.parseDouble(amount3)+ Double.parseDouble(amount4)+ Double.parseDouble(amount5);
			double rounded = (double) Math.round(sumOfAmount * 100) / 100;
			Assert.assertEquals(totalAmount, rounded);
			logger.info("Total Amount match");
			assert true;
		} else {
			logger.info("Total Amount is not matching");
			assert false;
			Assert.fail();
		}

		if (checkElementPresence(viewDetail)) {
			String fileName = "billing-details.pdf";
			moveToWebElementAndActionClick(viewDetail);
			Thread.sleep(2000);
			checkFileDownload(fileName);
			logger.info(fileName + " Document is downloaded successfully");
			assert true;
		} else {
			Assert.fail();
		}

		// click on search button and verify true vision
		if (accessbutton(searchButton)) {
			String planNameOnPopup = getText(popPlanName);
			assertString(textFromApplication(tueVisionNumberOnPopUp), trueVisionNumberOnCard);
			//assertString(textFromApplication(hdChannelsOnPopup), hdChannelText);
			//assertString(textFromApplication(channelsOnPopup), channelText);
			Assert.assertEquals(planNameOnPopup, planNameOnCard);
			waitFindClick(crossPackage);
			assert true;
		} else {
			assert false;
		}
	}

}
