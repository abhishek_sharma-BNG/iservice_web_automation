package com.automation.iService.utils;

import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class TrueVerifyTopUPForPrepaid extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueVerifyTopUPForPrepaid.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By billPayContinue = By.id("payment_mode_selection_popup_pay_button_button");
	By prepaid = By.id("bills_usage_prepaid");
	By twentyAmount = By.id("bills_usage_prepaid_amount_group-0");
	By radio = By.xpath("//*[@id='bills_usage_acc_card_option_block_prepaid_0']/div/div/label");
	By topUp = By.id("bills_usage_prepaid_topup_button_button");
	
	String twntyAmount;
	Double amount;

	// Method to verify bill payment on Pre-paid account
	public void verifyTopUpOnPrepaid(Entry<String, List<String>> values) throws Exception {

	}
}
