package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TruePreloginBillsUsage extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TruePreloginBillsUsage.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By postpaid = By.xpath("//*[@id=\"bills_usage_billing_summary\"]/span");
	By trueOnlineNumber0 = By.id("bills_usage_acc_card_product_value_tol_0");
	By balance = By.id("checkbox_amount_tol_0");
	By planName = By.id("tol_0_price_plan");
	By currentUsage = By.id("bills_usage_online_tab_view_0_first_label");
	By downloadMBPS = By.id("bills_usage_online_usage_0_download_speed");
	By uploadMBPS = By.id("bills_usage_online_usage_0_upload_speed");
	By billDetail = By.id("bills_usage_online_tab_view_0_second_label");
	By searchButton = By.id("tol_0_search");
	By trueOnlineNumberOnPopUp = By.id("popupPanel_productValue");
	By popPlanName = By.id("tol_0bills_usage_online_details_package_name");
	By downloadSpeedOnPopup = By.id("tol_0bills_usage_online_details_package_download_value_label");
	By uploadSpeedOnPopup = By.id("tol_0bills_usage_online_details_package_upload_value_label");
	By crossPackage = By.id("popupPanel_closeIcon");
	By headerLogo = By.id("header_signup_login_signupbarLogo");

	String trueOnlineNumberOnCard, planNameOnCard, downloadSpeedText, uploadSpeedText, uploadSpeed, amnt0, getText0,
			amnt1, getText1, amnt2, getText2, amnt3, getText3, amnt4, getText4;
	Double totalAmount;

	// Method to verify True online account
	public void verifyBillsUsage() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(1000);
				retryCount++;
			}
		}

		if (textFromApplication(postpaid).equalsIgnoreCase("Billing Summary")) {
			logger.info("Postpaid section is selected");
			assert true;
		} else {
			logger.info("Postpaid section is not selected");
			assert false;
		}
		Thread.sleep(1000);
		if (checkElementPresence(trueOnlineNumber0)) {
			waitForElement(trueOnlineNumber0);
			trueOnlineNumberOnCard = getText(trueOnlineNumber0);
			assert true;
		} else {
			assert false;
		}
		
		waitForElement(planName);
		planNameOnCard = getText(planName);
		if (checkElementPresence(balance)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(planName)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(currentUsage)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(downloadMBPS)) {
			assert true;
		} else {
			assert false;
		}

		// click on search button and verify true online
		if (accessbutton(searchButton)) {
			String planNameOnPopup = getText(popPlanName);
			assertString(textFromApplication(trueOnlineNumberOnPopUp), trueOnlineNumberOnCard);
			// assertString(textFromApplication(downloadSpeedOnPopup), downloadSpeedText);
			// assertString(textFromApplication(uploadSpeedOnPopup), uploadSpeedText);
			Assert.assertEquals(planNameOnPopup, planNameOnCard);
			waitFindClick(crossPackage);
			if (checkElementPresence(headerLogo)) {
				javaScriptScrollUsingBy(headerLogo);
				accessbutton(headerLogo);
				Assert.assertEquals(props.getProperty("LandingPageUrl"), driver.getCurrentUrl());
			}
			
			assert true;
		} else {
			assert false;
		}
	}

}
