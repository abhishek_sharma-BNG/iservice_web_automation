package com.automation.iService.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;

public class TrueCreditCardList extends TrueBillPaymentAndSaveCreditCard {

	static final Logger LOGGER = Logger.getLogger(TrueCreditCardList.class.getName());

	// Map for credit card
	Map<String, List<String>> paymentTestData = new HashMap<String, List<String>>();
	// List for credit card
	List<String> objectCard = new ArrayList<String>();

	// created constructor and adding credit card list
	public TrueCreditCardList() {
		
		
		// card1
		paymentTestData.put(TestData.VISACARD1, objectCard);
		// card2
		paymentTestData.put(TestData.VISACARD2, objectCard);
		// card3
		paymentTestData.put(TestData.MASTERCARD1, objectCard);
		// card4
		paymentTestData.put(TestData.JCBCARD1, objectCard);

		objectCard.add(TestData.CARDHOLDERNAME);
		objectCard.add(TestData.EXPIRY);
		objectCard.add(TestData.CVV3);
	}

	public Map<String, List<String>> getpaymentTestData() {
		return paymentTestData;
	}
	
	public void setpaymentTestData(Map<String, List<String>> paymentTestData) {
		this.paymentTestData = paymentTestData;
	}

	// Method to passing card values
	public void verifyPaymentGatewayforPrepaid() throws Exception {
		
		Map<String, List<String>> paymentTestData = getpaymentTestData();

		for (Entry<String, List<String>> iterable_element : paymentTestData.entrySet()) {
			verifyBillPaymentViaPrepaid(iterable_element);
		}
	}
}
