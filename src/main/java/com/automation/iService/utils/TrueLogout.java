package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TrueLogout extends CommonUtils{
	
	static final Logger LOGGER = Logger.getLogger(TrueLogout.class.getName());

	// Objects
	By logout = By.id("header_signup_login_logout");
	
	// Method to logout account
	public void logoutAccount() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(logout)) {
			Thread.sleep(1000);
			moveToWebElementAndActionClick(logout);
			logger.info("Logout successfully");
			assert true;
		} else {
			logger.info("Not login");
		}
	}
}
