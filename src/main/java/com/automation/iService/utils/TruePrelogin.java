package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class TruePrelogin extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TruePrelogin.class.getName());

	// Objects
	By textBox = By.id("login_text_box_textbox_field");
	By checkUsage = By.xpath(
			"//img[@src='https://s3-ap-southeast-1.amazonaws.com/isv2.dmpcdn.com/iservice_v2/web/assets/check.png']");
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By userPass = By.id("password");
	By frame = By.className("fancybox-iframe");
	By loginButton = By.id("bt_signin");
	By existingEmail = By.xpath("//*[contains(@class,'member-name')]");
	By enLanguage = By.id("header_change_language_th}");

	// Method to enter number and click on button
	public void prelogin(String uuID) throws Exception {

		waitForPageLoad();
		
		if (checkElementPresence(textBox)) {
			// sending true online number
			accessbutton(enLanguage);
			waitFindEnterText(textBox, uuID);
		
			assert true;
		} else {
			assert false;
		}

		if (checkElementPresence(checkUsage)) {
			accessbutton(checkUsage);
			if (checkElementPresence(loaderSpinner)) {
				int retryCount = 0;
				while (checkElementPresence(loaderSpinner) == true
						&& retryCount <= Integer.parseInt(props.getProperty("maxRetryCount"))) {
					hardStop(1000);
					retryCount++;
				}
			}
		} else {
			assert false;
		}
		waitForPageLoad();
		hardStop(10000);

	}

}
