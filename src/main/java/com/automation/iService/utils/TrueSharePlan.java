package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TrueSharePlan extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueSharePlan.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By postpaid = By.xpath("//*[@id=\"bills_usage_billing_summary\"]/span");
	By TMH = By.id("bills_usage_acc_card_product_type_postpaid_0");
	By chevronPostpaid0 = By.id("postpaid_0_chevron");
	//By chevronPostpaid1 = By.id("postpaid_1_chevron");
	By sharePlan = By.className("PWZtZibTZnwRmju29uS1S");
	By number1 = By.className("_21ufueCa0Kl6MkEVReYMTZ");
//	By number2 = By.id("bills_usage_shared_sim_1");	
	By crossSim = By.className("_3qbUG8eHe4A5rBOLYLKoUV");
	
	String postpaidNumberOnCard, planNameOnCard, planNameOnPop;

	// Method to verify share plan in postpaid account
	public void sharePlanPostpaidAccount() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(1000);
				retryCount++;
			}
		}
		if (textFromApplication(postpaid).equalsIgnoreCase("Billing Summary")) {
			logger.info("Postpaid section is selected");
			assert true;
		} else {
			logger.info("Postpaid section is not selected");
			assert false;
		}
		// check the presence of Suspended, if Suspended is not presence click on chevron button
		if (checkElementPresence(TMH)) {
			Thread.sleep(2000);
				accessbutton(chevronPostpaid0);
				logger.info("Clicked on first chevron button");
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(750);
						retryCount++;
					}
				}
				assert true;
			}
		
		Thread.sleep(1000);
		
		// click on share plan
		if (accessbutton(sharePlan)) {
			checkElementPresence(number1);
			//checkElementPresence(number2);
			waitFindClick(crossSim);
			assert true;
		} else {
			assert false;
		}
	}

}
