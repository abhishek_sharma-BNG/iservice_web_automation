package com.automation.iService.utils;

import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TruePreLoginPayTopUp extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueBillPaymentAndSaveCreditCard.class.getName());

	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By payTopUp = By.id("login_pay_button_button");
	By errorMessage = By.id("login_text_box_textbox_error_line");
	
	By billPayContinue = By.id("payment_mode_selection_popup_pay_button_button");
	By radio = By.xpath("//*[@id='bills_usage_acc_card_option_block_prepaid_0']/div/div/label");
	By topUp = By.id("bills_usage_prepaid_topup_button_button");
	By otherCards1 = By.id("card-list-radio-1_label");
	By otherCards2 = By.id("card-list-radio-2_label");
	By otherCards3 = By.id("card-list-radio-3_label");
	By otherCards4 = By.id("card-list-radio-4_label");
	By totalAmountOnPopUp = By.id("payment_popup_amount");
	By cardNo = By.id("payment_popup_cardNumber_input_textbox_field");
	By cardHolderName = By.id("payment_popup_cardName_input_textbox_field");
	By expiryDate = By.id("payment_popup_cardDate_input_textbox_field");
	By CCVNumber = By.id("payment_popup_cvvNumber_input_textbox_field");
	By saveThisCard = By.id("checkbox_payment_card_save_card_checkbox");
	By payButton = By.id("payment_popup_pay_button_button");
	By payConfirm = By.id("payment_confirmation_amount");
	By viewDetailButton = By.id("payment_confirmation_view_details}_button");
	By transactionNo = By.id("payment_receipt_transcation_number_value");
	By PaymentDate = By.id("payment_receipt_transcation_payment_date");
	By service = By.id("payment_receipt_transcation_service_type_value_0_prodType");
	By serviceNo = By.id("payment_receipt_transcation_service_number_value_0_prodValue");
	By statusOfTransaction = By.id("payment_receipt_transcation_status_value");
	By cross = By.xpath("//img[@src='/cc8fdab6a500e9da6c4bed4d1651ba45.svg']");
	By confirm = By.id("payment_confirmation_confirm}_button");

	// Method to verify PayTopUp 
	public void verifyBillPaymentViaPrepaid() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(750);
				retryCount++;
			}
		}
		if (checkElementPresence(payTopUp)) {
			waitFindClick(payTopUp);
			Assert.assertEquals(textFromApplication(errorMessage), "Invalid Service Number");
		} else {
			logger.info("Not able to see error message");
			Assert.fail();
			assert false;
		}
	}
	
	
}
