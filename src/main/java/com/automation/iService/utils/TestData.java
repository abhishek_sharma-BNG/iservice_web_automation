package com.automation.iService.utils;

import org.apache.log4j.Logger;

public class TestData extends TestBase{

		//TestData for Login
		static final Logger LOGGER = Logger.getLogger(TestData.class.getName());
		public final static String PREPAID = "0866626411";
		public final static String POSTPAID = "0937965039";
		public final static String BILLPOSTPAID = "0971400953";
		public final static String BILLPREPAID = "0866626411";
		public final static String SHAREPLAN = "0917904615";
		public final static String MULTIPLESIM = "0916961018";
		public final static String EBILLTRUEONLINE = "0866626411";
		public final static String TRUEONLINE = "0937965027";
		public final static String TRUEVISION = "0847223735";
		public final static String BILLINGMETHOD = "0816864236";
		public final static String PASS = "11111111";
		public final static String CHECKUSAGETOL = "NF0043135I";
		public final static String PAYNOW = "9610000128";
		
		//TestData for Credit cards
		public final static String VISACARD1 = "4242424242424242";
		public final static String VISACARD2 = "4111111111111111";
		public final static String MASTERCARD1 = "5555555555554444";
		public final static String MASTERCARD2 = "5454545454545454";
		public final static String JCBCARD1 = "3530111333300000";
		public final static String JCBCARD2 = "3566111111111113";
		public final static String CARDHOLDERNAME = "USER";
		public final static String EXPIRY = "12/21";
		public final static String CVV3 = "123";
		public final static String CVV4 = "1234";
		
		//TestData for emails
		public final static String EMAIL = "user1@user.com";
		public final static String SMS = "1234567890";
		
}
