package com.automation.iService.utils;

import static org.testng.Assert.assertNotEquals;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

public class TruePrepaid extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TruePrepaid.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By prepaid = By.id("bills_usage_prepaid_numbers");
	By prepaidNumber = By.id("bills_usage_acc_card_product_value_prepaid_0");
	By validityDate = By.id("bills_usage_acc_card_product_active_until_prepaid_0");
	By remainingDays = By.id("bills_usage_acc_card_days_text_prepaid_0");
	By balance = By.id("prepaid_radio_0_amount");
	By chevronPrepaid = By.id("prepaid_0_chevron");
	By planName = By.id("prepaid_0_price_plan");
	By extraPackage = By.id("prepaid_0_extra_package");
	By currentUsage = By.id("bills_usage_prepaid_tab_view_0_first_label");
	By searchButton = By.id("prepaid_0_price_plan");
	By usageDetail = By.id("prepaid_0_first_label");
	By buyButton = By.xpath("//*[@id=\"extra_package_no_extra_package_button\"]/div");
	By mobilenumberOnPopup = By.id("popupPanel_productValue");
	By crossPackage = By.id("popupPanel_closeIcon");
	By noCurrentUsage = By.xpath("//*[@id=\"bills_usage_prepaid_tab_view_0_first_content\"]/div/div");

	String numberPrepaid, nocurrentUsage;

	// Method to verify prepaid AC
	public void prepaidAccount() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(750);
				retryCount++;
			}
		}
		if (textFromApplication(prepaid).equalsIgnoreCase("Prepaid numbers")) {
			logger.info("Prepaid section is selected");
			assert true;
		} else {
			logger.info("Prepaid section is not selected");
			assert false;
		}

		// click on chevron button
		if (accessbutton(chevronPrepaid)) {
			Thread.sleep(2000);
			if (checkElementPresence(loaderSpinner)) {
				int retryCount = 0;
				while (checkElementPresence(loaderSpinner) == true
						&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
					Thread.sleep(750);
					retryCount++;
				}
			}
			if (checkElementPresence(prepaidNumber)) {
				numberPrepaid = getText(prepaidNumber);
				assert true;
			} else {
				assert false;
			}
			if (checkElementPresence(validityDate)) {
				assert true;
			} else {
				assert false;
			}
			if (checkElementPresence(remainingDays)) {
				assert true;
			} else {
				assert false;
			}
			if (checkElementPresence(balance)) {
				assert true;
			} else {
				assert false;
			}
			if (checkElementPresence(planName)) {
				assert true;
			} else {
				assert false;
			}
			if (checkElementPresence(extraPackage)) {
				assert true;
			} else {
				assert false;
			}
			if (checkElementPresence(currentUsage)) {
				assert true;
			} else {
				assert false;
			}
			String planNameOnCard = getText(planName);
			nocurrentUsage = getText(noCurrentUsage);
			Assert.assertNotEquals(noCurrentUsage, "No current usage");
			assert true;
		} else {
			assert false;
		}

		// click on search button and verify current usage and plan
		if (accessbutton(searchButton)) {
			Thread.sleep(2000);
			if (checkElementPresence(usageDetail)) {
				assert true;
			} else {
				assert false;
			}
			if (checkElementPresence(buyButton)) {
				assert true;
			} else {
				assert false;
			}
			String numberOnPopup = getText(mobilenumberOnPopup);
			Assert.assertEquals(numberOnPopup, numberPrepaid);
			moveToWebElementAndActionClick(crossPackage);
			assert true;
		} else {
			assert false;
		}
	}

}
