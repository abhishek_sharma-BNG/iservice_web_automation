package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TrueOnline extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueOnline.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By postpaid = By.xpath("//*[@id=\"bills_usage_billing_summary\"]/span");
	By trueOnlineNumber0 = By.id("bills_usage_acc_card_product_value_tol_0");
	By trueOnlineNumber = By.id("bills_usage_acc_card_product_value_tol_1");
	By chevronButton = By.id("tol_1_chevron");
	By noOverdueBills = By.xpath("//*[@id=\"bills_usage_online_tab_view_1_second_content\"]/div/div[1]/div");
	By balance = By.id("checkbox_amount_tol_1");
	By planName = By.id("tol_1_price_plan");
	By currentUsage = By.id("bills_usage_online_tab_view_0_first_label");
	By downloadMBPS = By.id("bills_usage_online_usage_1_download_speed");
	By uploadMBPS = By.id("bills_usage_online_usage_1_upload_speed");
	By billDetail = By.id("bills_usage_online_tab_view_1_second_label");
	By amount0 = By.id("checkbox_amount_bills_usage_online_bill_detail_1-invoice-checkbox-0");
	By amount1 = By.id("checkbox_amount_bills_usage_online_bill_detail_1-invoice-checkbox-1");
	By amount2 = By.id("checkbox_amount_bills_usage_online_bill_detail_1-invoice-checkbox-2");
	By amount3 = By.id("checkbox_amount_bills_usage_online_bill_detail_1-invoice-checkbox-3");
	By amount4 = By.id("checkbox_amount_bills_usage_online_bill_detail_1-invoice-checkbox-4");
	By viewDetail = By.id("bills_usage_online_bill_detail_0-invoice-month-1_view_details");
	By searchButton = By.id("tol_1_search");
	By trueOnlineNumberOnPopUp = By.id("popupPanel_productValue");
	By popPlanName = By.id("tol_1bills_usage_online_details_package_name");
	By downloadSpeedOnPopup = By.id("tol_1bills_usage_online_details_package_download_value_label");
	By uploadSpeedOnPopup = By.id("tol_1bills_usage_online_details_package_upload_value_label");
	By crossPackage = By.id("popupPanel_closeIcon");

	String trueOnlineNumberOnCard, planNameOnCard, downloadSpeedText, uploadSpeedText, uploadSpeed, amnt0, getText0,
			amnt1, getText1, amnt2, getText2, amnt3, getText3, amnt4, getText4;
	Double totalAmount;

	// Method to verify True online account
	public void trueOnlineAccount() throws Exception {
		
		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(1000);
				retryCount++;
			}
		}

		if (textFromApplication(postpaid).equalsIgnoreCase("Billing Summary")) {
			logger.info("Postpaid section is selected");
			assert true;
		} else {
			logger.info("Postpaid section is not selected");
			assert false;
		}
		Thread.sleep(1000);
		if (checkElementPresence(trueOnlineNumber)) {
			waitForElement(trueOnlineNumber);
			trueOnlineNumberOnCard = getText(trueOnlineNumber);
			assert true;
		} else if(checkElementPresence(trueOnlineNumber0)){
			waitForElement(trueOnlineNumber0);
			trueOnlineNumberOnCard = getText(trueOnlineNumber0);
			assert true;
		}

		// opening card after click on chevron button
		if (accessbutton(chevronButton)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(1000);
				retryCount++;
			}
		}
		waitForElement(planName);
		planNameOnCard = getText(planName);
		if (checkElementPresence(balance)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(planName)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(currentUsage)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(downloadMBPS)) {
			assert true;
		} else {
			assert false;
		}
		String downloadSpeed = getText(downloadMBPS);
		downloadSpeedText = downloadSpeed.substring(0, downloadSpeed.length() - 2);
		if (checkElementPresence(uploadMBPS)) {
			assert true;
		} else {
			assert false;
		}
		String uploadSpeed = getText(uploadMBPS);
		uploadSpeedText = uploadSpeed.substring(0, uploadSpeed.length() - 2);
		if (checkElementPresence(billDetail)) {
			assert true;
		} else {
			assert false;
		}

		if (checkElementPresence(balance)) {
			String amount = getText(balance);
			String tAmount = amount.replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);

			if (checkElementPresence(amount0)) {
				getText0 = getText(amount0);
				amnt0 = getText0.replaceAll(",", "");
				if (checkElementPresence(amount1)) {
					getText1 = getText(amount1);
					amnt1 = getText1.replaceAll(",", "");
				}
				double sumOfAmount = Double.parseDouble(amnt0) + Double.parseDouble(amnt1);
				double rounded = (double) Math.round(sumOfAmount * 100) / 100;
				Assert.assertEquals(totalAmount, rounded);
				logger.info("Total Amount match");

			} else if (checkElementPresence(noOverdueBills)) {
				getText(noOverdueBills);
			}

			assert true;
		} else {
			logger.info("Total Amount is not matching");
			assert false;
			Assert.fail();
		}

		if (checkElementPresence(viewDetail)) {
			String fileName = "billing-details.pdf";
			moveToWebElementAndActionClick(viewDetail);
			Thread.sleep(2000);
			checkFileDownload(fileName);
			logger.info(fileName + " Document is downloaded successfully");
			assert true;
		} else if (checkElementPresence(noOverdueBills)) {
			getText(noOverdueBills);
		}

		// click on search button and verify true online
		if (accessbutton(searchButton)) {
			String planNameOnPopup = getText(popPlanName);
			assertString(textFromApplication(trueOnlineNumberOnPopUp), trueOnlineNumberOnCard);
			// assertString(textFromApplication(downloadSpeedOnPopup), downloadSpeedText);
			// assertString(textFromApplication(uploadSpeedOnPopup), uploadSpeedText);
			Assert.assertEquals(planNameOnPopup, planNameOnCard);
			waitFindClick(crossPackage);
			assert true;
		} else {
			assert false;
		}
	}
}