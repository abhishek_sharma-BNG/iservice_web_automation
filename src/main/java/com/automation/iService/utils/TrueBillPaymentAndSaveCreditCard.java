package com.automation.iService.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TrueBillPaymentAndSaveCreditCard extends CommonUtils {

	TrueVerifyTopUPForPrepaid amountCheck = new TrueVerifyTopUPForPrepaid();

	static final Logger LOGGER = Logger.getLogger(TrueBillPaymentAndSaveCreditCard.class.getName());

	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By prepaid = By.xpath("//*[@id=\"bills_usage_prepaid_numbers\"]/span");
	By twentyAmount = By.id("bills_usage_prepaid_amount_group-0");
	By billPayContinue = By.id("payment_mode_selection_popup_pay_button_button");
	By radio = By.id("prepaid_radio_0_label");
	By topUp = By.id("bills_usage_prepaid_topup_button_button");
	By otherCards1 = By.id("card-list-radio-1_label");
	By otherCards2 = By.id("card-list-radio-2_label");
	By otherCards3 = By.id("card-list-radio-3_label");
	By otherCards4 = By.id("card-list-radio-4_label");
	By totalAmountOnPopUp = By.id("payment_popup_amount");
	By cardNo = By.id("payment_popup_cardNumber_input_textbox_field");
	By cardHolderName = By.id("payment_popup_cardName_input_textbox_field");
	By expiryDate = By.id("payment_popup_cardDate_input_textbox_field");
	By CCVNumber = By.id("payment_popup_cvvNumber_input_textbox_field");
	By saveThisCard = By.id("checkbox_payment_card_save_card_checkbox");
	By payButton = By.id("payment_popup_pay_button_button");
	By payConfirm = By.id("payment_confirmation_amount");
	By viewDetailButton = By.id("payment_confirmation_view_details}_button");
	By transactionNo = By.id("payment_receipt_transcation_number_value");
	By PaymentDate = By.id("payment_receipt_transcation_payment_date");
	By service = By.id("payment_receipt_transcation_service_type_value_0_prodType");
	By serviceNo = By.id("payment_receipt_transcation_service_number_value_0_prodValue");
	By statusOfTransaction = By.id("payment_receipt_transcation_status_value");
	By cross = By.xpath("//img[@src='/cc8fdab6a500e9da6c4bed4d1651ba45.svg']");
	By confirm = By.id("payment_confirmation_confirm}_button");

	String twntyAmount;
	Double amount;

	// Method to verify bill payment on Pre-paid account by three different cards
	public void verifyBillPaymentViaPrepaid(Entry<String, List<String>> values) throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
				Thread.sleep(750);
				retryCount++;
			}
		}
		if (textFromApplication(prepaid).equalsIgnoreCase("Prepaid numbers")) {
			logger.info("Prepaid section is selected");
			assert true;
		} else {
			logger.info("Prepaid section is not selected");
			assert false;
		}
		
		// click on topup
		if (accessbutton(twentyAmount)) {
			twntyAmount = getText(twentyAmount);
			amount = Double.parseDouble(twntyAmount);
			Thread.sleep(1000);
			moveToWebElementAndActionClick(radio);
			if (checkElementPresence(topUp)) {
				waitFindClick(topUp);
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(500);
						retryCount++;
					}
				}
			}

			if (accessbutton(billPayContinue)) {

				if (checkElementPresence(otherCards1)) {
					waitFindClick(otherCards1);
				} else if (checkElementPresence(otherCards2)) {
					waitFindClick(otherCards2);
				} else if (checkElementPresence(otherCards3)) {
					waitFindClick(otherCards3);
				} else if (checkElementPresence(otherCards4)) {
					waitFindClick(otherCards4);
				}

				List<String> listValue = values.getValue();
				Assert.assertEquals(textFromApplicationAndConvertToDouble(totalAmountOnPopUp), amount);
				waitFindEnterText(cardNo, values.getKey());
				// use list iterator here
				Iterator<String> IterlistValue = listValue.iterator();

				while (IterlistValue.hasNext()) {

					waitFindEnterText(cardHolderName, IterlistValue.next().toString());
					waitFindEnterText(expiryDate, IterlistValue.next().toString());
					waitFindEnterText(CCVNumber, IterlistValue.next().toString());
				}
				javaScriptCheckBox(saveThisCard);
			}
			if (accessbutton(payButton)) {
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(500);
						retryCount++;
					}
				}
			}
			if (checkElementPresence(payConfirm)) {
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(500);
						retryCount++;
					}
				}
				Assert.assertEquals(textFromApplicationAndConvertToDouble(payConfirm), amount);
				Thread.sleep(1000);
				if (accessbutton(viewDetailButton)) {
					checkElementPresence(transactionNo);
					checkElementPresence(PaymentDate);
					checkElementPresence(service);
					checkElementPresence(serviceNo);
					checkElementPresence(statusOfTransaction);
					// Assert.assertEquals(textFromApplicationAndConvertToDouble(paymentAmountOnReceipt),amount);
					accessbutton(cross);
					Thread.sleep(2000);
				} else {
					Assert.fail();
					assert false;
				}
			} else {
				Assert.fail();
				assert false;
			}
		} else {
			Assert.fail();
			assert false;
		}
	}
}