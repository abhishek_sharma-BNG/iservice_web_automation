package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TruePostpaid extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TruePostpaid.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By postpaid = By.xpath("//*[@id=\"bills_usage_billing_summary\"]/span");
	By postpaidNumber = By.className("MiCE8QHAyZv2EKUSg0IEF");
	By balance = By.id("checkbox_amount_postpaid_0");
	By planName = By.id("postpaid_0_price_plan");
	By extraPackage = By.id("postpaid_0_extra_package");
	By currentUsage = By.id("bills_usage_postpaid_tab_view_0_first_label");
	By voiceUsage = By.xpath("//*[@id=\"bills_usage_postpaid_current_usage_0_voice-val-text\"]");
	By dataValue = By.id("bills_usage_postpaid_current_usage_0_data-val-text");
	By totalVoiceUsageInMinutes = By.id("bills_usage_postpaid_current_usage_0_voice-val-out-of-text");
	By amount0 = By.id("checkbox_amount_bills_usage_postpaid_bill_detail_0-invoice-checkbox-0");
	By amount1 = By.id("checkbox_amount_bills_usage_postpaid_bill_detail_0-invoice-checkbox-1");
	By amount2 = By.id("checkbox_amount_bills_usage_postpaid_bill_detail_0-invoice-checkbox-2");
	By amount3 = By.id("checkbox_amount_bills_usage_postpaid_bill_detail_0-invoice-checkbox-3");
	By billDetail = By.id("bills_usage_postpaid_tab_view_0_second_label");
	By billDetaillink = By.id("bills_usage_postpaid_bill_detail_0-invoice-month-0_view_details");
	By searchButton = By.id("postpaid_0_search");
	By postPaidNumberOnPopUp = By.id("popupPanel_productValue");
	By wifiUnlimitedPD = By.id("postpaid_0bills_usage_package_details_wifi_planFeatureImg_0_value");
	By dataUnlimitedPD = By.id("postpaid_0bills_usage_package_details_data_planFeatureContent_1_value");
	By popPlanName = By.id("postpaid_0bills_usage_package_details_packageName");
	By voiceMinute = By.id("postpaid_0bills_usage_package_details_voice_planFeatureContent_2_value");
	By creditLimit = By.id("postpaid_0bills_usage_package_details_credit_value");
	By packageDetail = By.id("undefined_first_label");
	By usageDetails = By.xpath("//*[@id='undefined_second_label']/label");
	By wifiUnlimitedUD = By.id("postpaid_0_bills_usage_extra_package_WIFI_0_unlimited");
	By dataUnlimitedUD = By.id("postpaid_0_bills_usage_extra_package_DATA_1_unlimited");
	By activeUsedMinute = By.id("postpaid_0_bills_usage_extra_package_VOICE_2_labelsExtraPackageValue");
	By activeRemainingMinute = By.id("postpaid_0_bills_usage_extra_package_VOICE_2_units");
	By inActiveUsedMinute = By.id("postpaid_0_bills_usage_extra_package_VOICE_0_labelsExtraPackageValue");
	By inActiveRemainingMinute = By.id("postpaid_0_bills_usage_extra_package_VOICE_0_units");
	By crossPackage = By.id("popupPanel_closeIcon");

	String postpaidNumberOnCard, planNameOnCard, planNameOnPop, dataValueText;
	Integer voiceUsageValue, voiceUsageValueInMin;
	Double totalAmount;

	// Method to verify postpaid account
	public void postpaidAccount() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount <= Integer.parseInt(props.getProperty("maxRetryCount"))) {
				hardStop(1000);
				retryCount++;
			}
		}
		
	
		hardStop(10000);
		if (textFromApplication(postpaid).equalsIgnoreCase("Billing Summary")) {
			logger.info("Postpaid section is selected");
			assert true;
		} else {
			logger.info("Postpaid section is not selected");
			assert false;
		}
		
		if (checkElementPresence(postpaidNumber)) {
			assert true;
		} else {
			assert false;
		}
		waitForElement(postpaidNumber);
		postpaidNumberOnCard = getText(postpaidNumber);
		waitForElement(planName);
		planNameOnCard = getText(planName);
		if (checkElementPresence(balance)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(planName)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(extraPackage)) {
			assert true;
		} else {
			assert false;
		}
		if (checkElementPresence(currentUsage)) {
			assert true;
		} else {
			assert false;
		}
		/*
		 * if(checkElementPresence(voiceUsage)) { assert true; } else { assert false; }
		 * if(checkElementPresence(dataValue)) { assert true; } else { assert false; }
		 * dataValueText = getText(dataValue); voiceUsageValue =
		 * textFromApplicationAndConvertToInteger(voiceUsage);
		 * if(checkElementPresence(totalVoiceUsageInMinutes)) { assert true; } else {
		 * assert false; } String voiceUsageMinute =
		 * textFromApplication(totalVoiceUsageInMinutes); String[] result =
		 * voiceUsageMinute.split(" "); String totalVoiceUsageInMinutes = result[1];
		 * voiceUsageValueInMin = Integer.parseInt(totalVoiceUsageInMinutes);
		 * checkElementPresence(billDetail);
		 */
		hardStop(1000);
		if (checkElementPresence(balance)) {
			hardStop(1000);
			String amount = getText(balance);
			String tAmount = amount.replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);
			if (checkElementPresence(amount0)) {
				String getText0 = getText(amount0);
				String amount0 = getText0.replaceAll(",", "");
				if (checkElementPresence(amount1)) {
					String getText1 = getText(amount1);
					String amount1 = getText1.replaceAll(",", "");
					if (checkElementPresence(amount2)) {
						String getText2 = getText(amount2);
						String amount2 = getText2.replaceAll(",", "");
						if (checkElementPresence(amount3)) {
							String getText3 = getText(amount3);
							String amount3 = getText3.replaceAll(",", "");
							double sumOfAmount = Double.parseDouble(amount0) + Double.parseDouble(amount1)
									+ Double.parseDouble(amount2) + Double.parseDouble(amount3);
							double rounded = (double) Math.round(sumOfAmount * 100) / 100;
							Assert.assertEquals(totalAmount, rounded);
							logger.info("Total Amount match");
							assert true;
						} else {
							assert false;
						}
					}
				}
			}
			assert true;
		} else {
			logger.info("Total Amount is not matching");
			assert false;
			Assert.fail();
		}

		if (checkElementPresence(billDetaillink)) {
			String fileName = "billing-details.pdf";
			moveToWebElementAndActionClick(billDetaillink);
			Thread.sleep(2000);
			checkFileDownload(fileName);
			logger.info(fileName + " Document is downloaded successfully");
			assert true;
		} else {
			Assert.fail();
		}

		// click on search button and verify package detail and usage Detail
		if (accessbutton(searchButton)) {
			checkElementPresence(packageDetail);
			String planOnPopup = getText(popPlanName);
			/*String split[] = planOnPopup.split(" ");
			planNameOnPop = split[1];*/
			assertString(textFromApplication(postPaidNumberOnPopUp), postpaidNumberOnCard);
			/*assertString(textFromApplication(wifiUnlimitedPD), dataValueText);
			assertString(textFromApplication(dataUnlimitedPD), dataValueText);*/
			Assert.assertEquals(planOnPopup, planNameOnCard);
			checkElementPresence(voiceMinute);
			if (accessbutton(usageDetails)) {
				checkElementPresence(usageDetails);
				Thread.sleep(2000);
				/*assertString(textFromApplication(wifiUnlimitedUD), dataValueText);
				assertString(textFromApplication(dataUnlimitedUD), dataValueText);
				// Getting current voice value from usage details and assert from current usage
				String getActiveText = getText(activeUsedMinute);
				String[] splitActiveText = getActiveText.split(" ");
				String arrSplitActiveText = splitActiveText[0];
				String amount0 = arrSplitActiveText.replaceAll(",", "");
				String getInActiveText = getText(inActiveUsedMinute);
				String[] splitInActiveText = getInActiveText.split(" ");
				String arrSplitInActiveText = splitInActiveText[0];
				String amount1 = arrSplitInActiveText.replaceAll(",", "");
				Integer sumActiveNInActiveMinute = Integer.parseInt(amount0) + Integer.parseInt(amount1);
				Assert.assertEquals(voiceUsageValue, sumActiveNInActiveMinute);
				// Getting Remaining voice value from usage details and assert from current
				// usage
				String getActiveRemainig = textFromApplication(activeRemainingMinute);
				String[] splitsActive = getActiveRemainig.split(" ");
				String totalActiveVoiceUsageInMinutes = splitsActive[1];
				String getInActiveRemainig = textFromApplication(inActiveRemainingMinute);
				String[] splitsInActive = getInActiveRemainig.split(" ");
				String totalInActiveRemainingVoiceUsageInMinutes = splitsInActive[1];
				Integer sumActiveNInActiveRemainingMinute = Integer.parseInt(totalActiveVoiceUsageInMinutes)
						+ Integer.parseInt(totalInActiveRemainingVoiceUsageInMinutes);
				Assert.assertEquals(voiceUsageValueInMin, sumActiveNInActiveRemainingMinute);*/
				// Bill and usage bill and over due
				waitFindClick(crossPackage);
			}
			assert true;
		} else {
			assert false;
		}
	}
}
