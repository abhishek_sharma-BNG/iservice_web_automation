package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class TrueHomePageLogin extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueHomePageLogin.class.getName());

	// Objects
	By signIn = By.className("_34S6z2pkb3kHDMZIvPrab_");
	By loginNumber = By.xpath("/html/body/div/div/form/div/div[2]/div[1]/div[2]/div/input");
	By userPass = By.id("password");
	By frame = By.className("fancybox-iframe");
	By loginButton = By.id("bt_signin");
	By existingEmail = By.xpath("//*[contains(@class,'member-name')]");
	By enLanguage = By.id("header_change_language_th}");

	// Method to login and Verify Email
	public void loginTrue(String uuID) throws InterruptedException {

		waitForPageLoad();
		if (accessbutton(enLanguage)) {
			accessbutton(signIn);
			assert true;
		} else {
			assert false;
		}
		waitForPageLoad();
		/*
		 * if
		 * (verifyTitle("3TrueID - ไอดีเดียว เติมเต็มความสุขทุกไลฟ์สไตล์ ที่เดียวจบ ครบทุกความต้องการ"
		 * )) { assert true; } else { assert false; }
		 */
		// sending email
		Thread.sleep(2000);
		switchInsideFrame(frame);
		Thread.sleep(500);
		// driver.switchTo().defaultContent();
		waitFindEnterText(loginNumber, uuID);
		// waiting for element
		if (waitForElement(userPass)) {
			assert true;
		} else {
			assert false;
		}
		// sending password
		waitFindEnterText(userPass, TestData.PASS);

		// Login
		if (checkElementdisplayed(loginButton)) {
			assert true;
		} else {
			LOGGER.info("Not able to login");
			assert false;
		}
		// click on login
		waitFindClick(loginButton);
		Thread.sleep(15000);

		// Assert.assertEquals(email, TestData.POSTPAID);
	}
}
