package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TrueTotalAmountOnPostpaid extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueTotalAmountOnPostpaid.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By postpaid = By.xpath("//*[@id=\"bills_usage_billing_summary\"]/span");
	By totalAmountPostpaid = By.id("bills_usage_postpaid_total_amount");
	By amountPostpaid5 = By.id("checkbox_amount_postpaid_5");
	By amountPostpaid4 = By.id("checkbox_amount_postpaid_4");
	By amountPostpaid3 = By.id("checkbox_amount_postpaid_3");
	By amountPostpaid2 = By.id("checkbox_amount_postpaid_2");
	By amountPostpaid1 = By.id("checkbox_amount_postpaid_1");
	By amountPostpaid0 = By.id("checkbox_amount_postpaid_0");
	By checkBox5 = By.id("checkbox_postpaid_5");
	By checkBox4 = By.id("checkbox_postpaid_4");
	By checkBox3 = By.id("checkbox_postpaid_3");
	By checkBox2 = By.id("checkbox_postpaid_2");
	By checkBox1 = By.id("checkbox_postpaid_1");

	String tAmount;
	Double totalAmount;

	// Method to verify billing method via postpaid account
	public void verifyTotalAmountOnPostpaid() throws Exception {

		waitForPageLoad();
		if (checkElementPresence(loaderSpinner)) {
			int retryCount = 0;
			while (checkElementPresence(loaderSpinner) == true
					&& retryCount < 25) {
				Thread.sleep(1000);
				retryCount++;
			}
		}
		
		if (textFromApplication(postpaid).equalsIgnoreCase("Billing Summary")) {
			logger.info("Postpaid section is selected");
			assert true;
		} else {
			logger.info("Postpaid section is not selected");
			assert false;
		}

		// check element presence
		Thread.sleep(2000);
		if (checkElementPresence(amountPostpaid5)) {
			String amount = getText(totalAmountPostpaid);
			tAmount = amount.substring(1).replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);
			String getText0 = getText(amountPostpaid0);
			String amount0 = getText0.replaceAll(",", "");
			String getText1 = getText(amountPostpaid1);
			String amount1 = getText1.replaceAll(",", "");
			String getText2 = getText(amountPostpaid2);
			String amount2 = getText2.replaceAll(",", "");
			String getText3 = getText(amountPostpaid3);
			String amount3 = getText3.replaceAll(",", "");
			String getText4 = getText(amountPostpaid4);
			String amount4 = getText4.replaceAll(",", "");
			String getText5 = getText(amountPostpaid5);
			String amount5 = getText5.replaceAll(",", "");
			double sumOfAmount = Double.parseDouble(amount0) + Double.parseDouble(amount1) + Double.parseDouble(amount2)
					+ Double.parseDouble(amount3) + Double.parseDouble(amount4) + Double.parseDouble(amount5);
			Assert.assertEquals(totalAmount, sumOfAmount);
			logger.info("Total Amount match with 6 cards");
			assert true;
		} else if (checkElementPresence(amountPostpaid4)) {
			String amount = getText(totalAmountPostpaid);
			tAmount = amount.substring(1).replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);
			String getText0 = getText(amountPostpaid0);
			String amount0 = getText0.replaceAll(",", "");
			String getText1 = getText(amountPostpaid1);
			String amount1 = getText1.replaceAll(",", "");
			String getText2 = getText(amountPostpaid2);
			String amount2 = getText2.replaceAll(",", "");
			String getText3 = getText(amountPostpaid3);
			String amount3 = getText3.replaceAll(",", "");
			String getText4 = getText(amountPostpaid4);
			String amount4 = getText4.replaceAll(",", "");
			double sumOfAmount = Double.parseDouble(amount0) + Double.parseDouble(amount1) + Double.parseDouble(amount2)
					+ Double.parseDouble(amount3) + Double.parseDouble(amount4);
			Assert.assertEquals(totalAmount, sumOfAmount);
			logger.info("Total Amount match with 5 cards");
			assert true;
		} else if (checkElementPresence(amountPostpaid3)) {
			String amount = getText(totalAmountPostpaid);
			tAmount = amount.substring(1).replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);
			String getText0 = getText(amountPostpaid0);
			String amount0 = getText0.replaceAll(",", "");
			String getText1 = getText(amountPostpaid1);
			String amount1 = getText1.replaceAll(",", "");
			String getText2 = getText(amountPostpaid2);
			String amount2 = getText2.replaceAll(",", "");
			String getText3 = getText(amountPostpaid3);
			String amount3 = getText3.replaceAll(",", "");
			double sumOfAmount = Double.parseDouble(amount0) + Double.parseDouble(amount1) + Double.parseDouble(amount2)
					+ Double.parseDouble(amount3);
			Assert.assertEquals(totalAmount, sumOfAmount);
			logger.info("Total Amount match with 4 cards");
			assert true;
		} else if (checkElementPresence(amountPostpaid2)) {
			String amount = getText(totalAmountPostpaid);
			tAmount = amount.substring(1).replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);
			String getText0 = getText(amountPostpaid0);
			String amount0 = getText0.replaceAll(",", "");
			String getText1 = getText(amountPostpaid1);
			String amount1 = getText1.replaceAll(",", "");
			String getText2 = getText(amountPostpaid2);
			String amount2 = getText2.replaceAll(",", "");
			double sumOfAmount = Double.parseDouble(amount0) + Double.parseDouble(amount1)
					+ Double.parseDouble(amount2);
			Assert.assertEquals(totalAmount, sumOfAmount);
			logger.info("Total Amount match with 3 cards");
			assert true;
		} else if (checkElementPresence(amountPostpaid1)) {
			String amount = getText(totalAmountPostpaid);
			tAmount = amount.substring(1).replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);
			String getText0 = getText(amountPostpaid0);
			String amount0 = getText0.replaceAll(",", "");
			String getText1 = getText(amountPostpaid1);
			String amount1 = getText1.replaceAll(",", "");
			double sumOfAmount = Double.parseDouble(amount0) + Double.parseDouble(amount1);
			Assert.assertEquals(totalAmount, sumOfAmount);
			logger.info("Total Amount match with 2 cards");
			assert true;
		} else if (checkElementPresence(amountPostpaid0)) {
			String amount = getText(totalAmountPostpaid);
			tAmount = amount.substring(1).replaceAll(",", "");
			totalAmount = Double.parseDouble(tAmount);
			String getText0 = getText(amountPostpaid0);
			String amount0 = getText0.replaceAll(",", "");
			double sumOfAmount = Double.parseDouble(amount0);
			Assert.assertEquals(totalAmount, sumOfAmount);
			logger.info("Total Amount match with 1 cards");
			assert true;

		} else {
			logger.info("Total amount not match");
			assert false;
		}

		/*if (checkElementPresence(checkBox5)) {

			if (accessbutton(checkBox5)) {
				accessbutton(checkBox5);
			} else {
				System.out.println("Not able to click Checkbox 5");
			}

		} else if (checkElementPresence(checkBox4)) {
			if (accessbutton(checkBox4)) {
				accessbutton(checkBox4);
			} else {
				System.out.println("Not able to click Checkbox 4");
			}

		} else if (checkElementPresence(checkBox3)) {
			if (accessbutton(checkBox4)) {
				accessbutton(checkBox4);
			} else {
				System.out.println("Not able to click Checkbox 3");
			}

		} else if (checkElementPresence(checkBox2)) {
			if (accessbutton(checkBox2)) {
				accessbutton(checkBox2);
			} else {
				System.out.println("Not able to click Checkbox 2");
			}

		} else if (checkElementPresence(checkBox1)) {
			if (accessbutton(checkBox1)) {
				assert true;
			} else {
				System.out.println("Not able to click Checkbox 1");
			}

		}*/

	}

}
