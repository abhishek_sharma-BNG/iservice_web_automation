package com.automation.iService.utils;

import org.openqa.selenium.WebDriver;

public class getDriver {
	public static WebDriver driver = null;

	public static WebDriver getDriver() {
		return driver;
	}

	public static  void setDriver(WebDriver driver) {
		getDriver.driver = driver;
	}
}
