package com.automation.iService.utils;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

public class TrueBillPaymentOnPostpaid extends CommonUtils {

	static final Logger LOGGER = Logger.getLogger(TrueBillPaymentOnPostpaid.class.getName());

	// Objects
	By loaderSpinner = By.className("s2-CHjfCqDFRzE0aDfB0C");
	By totalAmount = By.id("bills_usage_postpaid_total_amount");
	By payNow = By.id("bills_usage_postpaid_paynow_button_button");
	By billPayContinue = By.id("payment_mode_selection_popup_pay_button_button");
	By totalAmountOnPopUp = By.id("payment_popup_amount");
	By cardNo = By.id("payment_popup_cardNumber_input_textbox_field");
	By cardHolderName = By.id("payment_popup_cardName_input_textbox_field");
	By expiryDate = By.id("payment_popup_cardDate_input_textbox_field");
	By CCVNumber = By.id("payment_popup_cvvNumber_input_textbox_field");
	By saveThisCard = By.id("checkbox_payment_card_save_card_checkbox");
	By payButton = By.id("payment_popup_pay_button_button");
	By payConfirm = By.id("payment_confirmation_amount");
	By viewDetailButton = By.id("payment_confirmation_view_details}_button");
	By paymentAmountOnReceipt = By.id("payment_receipt_transcation_amount_int");
	By cross = By.xpath("//*[@id=\"app\"]/div/div/div[3]/div/div[2]/div[3]/div/span");
	By confirm = By.xpath("//img[@src='https://s3-ap-southeast-1.amazonaws.com/isv2.dmpcdn.com/iservice_v2/web/assets/ic_close.png']");

	String totalAmountOnCard;
	
	// Method to verify bill payment on Postpaid account
	public void verifyBillPaymentViaPostpaid() throws Exception {

		if (checkElementPresence(payNow)) {
			String amount = getText(totalAmount);
			totalAmountOnCard = amount.substring(1);
			moveToWebElementAndActionClick(payNow);
			if (checkElementPresence(loaderSpinner)) {
				int retryCount = 0;
				while (checkElementPresence(loaderSpinner) == true
						&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
					Thread.sleep(2000);
					retryCount++;
				}
			}
		
			// click on bill pay continue button
			Thread.sleep(2000);
			if (accessbutton(billPayContinue)) {
				assertString(textFromApplication(totalAmountOnPopUp), totalAmountOnCard);
				waitFindEnterText(cardNo, TestData.VISACARD1);
				waitFindEnterText(cardHolderName, TestData.CARDHOLDERNAME);
				waitFindEnterText(expiryDate, TestData.EXPIRY);
				waitFindEnterText(CCVNumber, TestData.CVV3);
				javaScriptCheckBox(saveThisCard);
			}
			if (accessbutton(payButton)) {
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(500);
						retryCount++;
					}
				}
			}
			if (checkElementPresence(payConfirm)) {
				if (checkElementPresence(loaderSpinner)) {
					int retryCount = 0;
					while (checkElementPresence(loaderSpinner) == true
							&& retryCount < Integer.parseInt(props.getProperty("maxRetryCount"))) {
						Thread.sleep(500);
						retryCount++;
					}
				}
				assertString(textFromApplication(payConfirm), totalAmountOnCard);
				Thread.sleep(1000);
				waitFindClick(viewDetailButton);
				assertString(textFromApplication(paymentAmountOnReceipt), totalAmountOnCard);
				Thread.sleep(1000);
				javaScriptClickUsingBy(cross);
				waitFindClick(confirm);
			}
		} else {
			Assert.fail();
			assert false;
		}
	}
}
