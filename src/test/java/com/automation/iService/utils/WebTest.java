package com.automation.iService.utils;

import java.io.IOException;
import java.lang.reflect.Method;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import com.relevantcodes.extentreports.LogStatus;

import atu.testrecorder.exceptions.ATUTestRecorderException;

public class WebTest extends TestBase {
	static final Logger logger = Logger.getLogger(WebTest.class.getName());

	By checkUsage = By.xpath(
			"//img[@src='https://s3-ap-southeast-1.amazonaws.com/isv2.dmpcdn.com/iservice_v2/web/assets/check.png']");
	By payNow = By.id("login_pay_button_button");

	TrueHomePageLogin objHomePage;
	TruePrepaid objTrueprepaid;
	TruePostpaid objPostpaid;
	TrueSharePlan objSharePlan;
	TrueMultipleSim objMultipleSim;
	TrueTotalAmountOnPostpaid objTotalAmount;
	TrueBillPaymentOnPostpaid objBillPaymentPost;
	TrueCreditCardList objBillPayment;
	TrueLogout objLogout;
	TrueVerifyTopUPForPrepaid objTopUpForPrepaid;
	TrueBillingMethod objBillingMethod;
	TrueVision objTrueVision;
	TrueOnline objTrueOnline;
	TruePrelogin objTruePrelogin;
	TruePreloginBillsUsage objBillsUsage;

	@BeforeSuite
	@Parameters("browser")

	public void setup(@Optional("chrome") String browser) throws IOException {
		super.setup(browser);
		objHomePage = new TrueHomePageLogin();
		objTrueprepaid = new TruePrepaid();
		objPostpaid = new TruePostpaid();
		objSharePlan = new TrueSharePlan();
		objMultipleSim = new TrueMultipleSim();
		objTotalAmount = new TrueTotalAmountOnPostpaid();
		objBillPaymentPost = new TrueBillPaymentOnPostpaid();
		objBillPayment = new TrueCreditCardList();
		objLogout = new TrueLogout();
		objTopUpForPrepaid = new TrueVerifyTopUPForPrepaid();
		objBillingMethod = new TrueBillingMethod();
		objTrueVision = new TrueVision();
		objTrueOnline = new TrueOnline();
		objTruePrelogin = new TruePrelogin();
		objBillsUsage = new TruePreloginBillsUsage();

	}

	@BeforeMethod
	public void beforeMethodSetup(Method method) {
		test = extent.startTest(method.getName().toString());
		test.assignCategory(browser);
		try {
			// String browserName = CommonUtils.getBrowserDetails();
			startRecording(method.getName().toString());

		} catch (ATUTestRecorderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// *************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************
	// TrueiService Test cases
	// Test cases -- Verify login and pre-paid section
	// @Test(priority = 1)
	public void verifyPrepaidAccount() throws Exception {
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.PREPAID);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objTrueprepaid.prepaidAccount();
			test.log(LogStatus.PASS, "Prepaid account verified successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Prepaid account was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Logout successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Logout was not verified successfully");
			assert (false);
		}
	}

	// Test cases -- Verify login and postpaid section
	// @Test(priority = 2)
	public void verifyPostpaidAccount() {
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.POSTPAID);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objPostpaid.postpaidAccount();
			test.log(LogStatus.PASS, "Postpaid account verified successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Postpaid account was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Logout successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Logout was not verified successfully");
			assert (false);
		}
	}

	// Test cases -- Verify login and TrueVision section
	// @Test(priority = 3)
	public void verifyBillsAndUsageTrueVisionAccount() {
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.TRUEVISION);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objTrueVision.trueVisionAccount();
			test.log(LogStatus.PASS, "TrueVision account verified successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "TrueVision account was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Logout successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Logout was not verified successfully");
			assert (false);
		}
	}

	// Test cases -- Verify login and TrueOnline section
	// @Test(priority = 4)
	public void verifyBillsAndUsageTrueOnlineAccount() {
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.TRUEONLINE);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objTrueOnline.trueOnlineAccount();
			test.log(LogStatus.PASS, "TrueVision account verified successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "TrueVision account was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Logout successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Logout was not verified successfully");
			assert (false);
		}
	}

	// Test cases -- Verify login and Share plan
	// @Test(priority = 5)
	public void verifySharePlanAccount() {
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.SHAREPLAN);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objSharePlan.sharePlanPostpaidAccount();
			test.log(LogStatus.PASS, "Share Plan verified successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification failure
			test.log(LogStatus.FAIL, "Share Plan was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Logout successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Logout was not verified successfully");
			assert (false);
		}
	}

	// Test cases -- Verify login and multiple sim section
	// @Test(priority = 6)
	public void verifyMultipleSimAccount() {
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.MULTIPLESIM);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objMultipleSim.multipleSimPostpaidAccount();
			test.log(LogStatus.PASS, "Multiple Sim verified successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Multiple Sim was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Logout successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Logout was not verified successfully");
			assert (false);
		}
	}

	// Test cases -- Verify bill payment via post paid
	// @Test(priority = 7)
	public void billPaymentViaPostpaid() {
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.BILLPOSTPAID);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objTotalAmount.verifyTotalAmountOnPostpaid();
			test.log(LogStatus.PASS, "Total Amount Validate");
		} catch (Exception e) {
			Assert.fail();// To fail test in case of any element identification failure
			test.log(LogStatus.FAIL, "Total Amount not Validate");
			assert (false);
		}
		try {
			objBillPaymentPost.verifyBillPaymentViaPostpaid();
			test.log(LogStatus.PASS, "Bill payment verify successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Bill payment was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
	}

	// Test cases -- Verify bill payment via Pre-paid
	// @Test(priority = 8)
	public void billPaymentViaPrepaid() {
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.BILLPREPAID);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objBillPayment.verifyPaymentGatewayforPrepaid();
			test.log(LogStatus.PASS, "Bill payment verify successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Bill payment was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
	}

	// Test cases -- Verify Edit E-bill
	// @Test(priority = 9)
	public void editEbillFromBillingMethod() {
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objHomePage.loginTrue(TestData.EBILLTRUEONLINE);
			test.log(LogStatus.PASS, "Login successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Login was not verified successfully");
			assert (false);
		}
		try {
			objBillingMethod.verifyEditEBill();
			test.log(LogStatus.PASS, "Bill payment verify successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Bill payment was not verified successfully");
			assert (false);
		}
		try {
			objLogout.logoutAccount();
			test.log(LogStatus.PASS, "Close session");
		} catch (Exception e) {
			test.log(LogStatus.FAIL, "Not able to close session");
		}
	}

	// Test cases -- Pre-login Verify after click on check usage
	// @Test(priority = 10)
	public void checkUsagePrelogin() {
		
		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objTruePrelogin.prelogin(TestData.CHECKUSAGETOL);
			test.log(LogStatus.PASS, "Prelogin successfully for Check Usage");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Check Usage was not verified successfully");
			assert (false);
		}
		try {
			objBillsUsage.verifyBillsUsage();
			test.log(LogStatus.PASS, "TrueOnline account verified successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "TrueOnline account was not verified successfully");
			assert (false);
		}
	}

	// Test cases -- Pre-login Verify after click on Pay Now
	@Test(priority = 11)
	public void payNowPrelogin() {

		try {
			loadUrl(props.getProperty("LandingPageUrl"), props.getProperty("Sitetitle"));
			objTruePrelogin.prelogin(TestData.PAYNOW);
			test.log(LogStatus.PASS, "Prelogin successfully for Pay Now");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Pay Now was not verified successfully");
			assert (false);
		}
		try {
			objBillPaymentPost.verifyBillPaymentViaPostpaid();
			test.log(LogStatus.PASS, "Bill payment verify successfully");
		} catch (Exception e) {
			Assert.fail(); // To fail test in case of any element identification
			// failure
			test.log(LogStatus.FAIL, "Bill payment was not verified successfully");
			assert (false);
		}
	}
}
